package com.regapp.regapp.controller;

import com.regapp.regapp.exception.ResourceNotFoundException;
import com.regapp.regapp.service.Student.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class AuthController {

    @Autowired
    StudentService studentService;

    @RequestMapping("/role")
    public Object getUserRole(Authentication authentication) {
        if(!authentication.getAuthorities().isEmpty()){
            return authentication.getAuthorities();
        } else {
            throw new ResourceNotFoundException("getAuthorities","User",authentication.getAuthorities());
        }
    }
}

import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native'
import { connect } from "react-redux";
import { logout } from "../../store/actions";
import ButtonWithBackground from '../../components/UI/Button/ButtonWithBackground';
class LogoutScreen extends Component {
    testFunction = key => {
        //Do nothing
    };

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <ButtonWithBackground
                    style={styles.button}
                    onPress={this.props.onLogout}
                >
                    <Text>Logg ut</Text>
                </ButtonWithBackground>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    button: {
        width: 200,
        height: 80,
        backgroundColor: '#4bacb8',
    }
});
const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch(logout())
    };
};

export default connect(null, mapDispatchToProps)(LogoutScreen);
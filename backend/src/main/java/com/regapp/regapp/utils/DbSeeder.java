package com.regapp.regapp.utils;


import com.regapp.regapp.model.Course;
import com.regapp.regapp.model.Lecturer;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
//@Service
public class DbSeeder implements CommandLineRunner {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private RequirementRepository requirementRepository;
    @Autowired
    private StudReqRepository studReqRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private LecturerRepository lecturerRepository;



    @Override
    @Transactional
    public void run(String... args) throws Exception {
        // Drop all content from db
       studentRepository.deleteAll();
       requirementRepository.deleteAll();
       studReqRepository.deleteAll();
       lecturerRepository.deleteAll();
        courseRepository.deleteAll();

        // Cleanup the tables
     //   studReqRepository.deleteAllInBatch();
      //  studentRepository.deleteAllInBatch();
      //  requirementRepository.deleteAllInBatch();

        Lecturer adrian1 = new Lecturer("Adrian Rutle","aru@hib.no");
        Lecturer adrian2 = new Lecturer("Adrian Rutle","aru@hvl.no");
        Lecturer adrian3 = new Lecturer("Adrian Rutle","Adrian.Rutle@hvl.no");
        Lecturer eva = new Lecturer("Eva Gerdts","ege042@uib.no");
        Lecturer eva2 = new Lecturer("Eva Gerdts","Eva.Gerdts@uib.no");
        Lecturer Ingeborg = new Lecturer("Ingeborg Eskerud","ies021@uib.no");
        Lecturer Hilde = new Lecturer("Hilde Halland","dak003@uib.no");
        Lecturer Ingeborg2 = new Lecturer("Ingeborg Eskerud","Ingeborg.Eskerud@uib.no");
        Lecturer Hilde2 = new Lecturer("Hilde Halland","Hilde.Halland@uib.no");
        Lecturer frankTest = new Lecturer("FrankTest","noreply@feide.no");

       //lecturerRepository.save(adrian);
        Student gard = new Student(
                "Gard Aaland Engen",
                "575532@stud.hvl.no"
                );
        Student gard2 = new Student(
                "Gard Aaland Engen",
                "Gard.Engen@student.uib.no"
        );
        Student trond = new Student(
                "Trond Are Mannsåker",
                "sez004@uib.no"
                );

        Student trond2 = new Student(
                "Trond Are Mannsåker",
                "Trond.Mannsaker@student.uib.no"
        );
        Student torbjorn = new Student(
                "Torbjørn Nordrik",
                "tno040@uib.no"
                );

        Student torbjorn2 = new Student(
                "Torbjørn Nordrik",
                "Torbjorn.Nordrik@student.uib.no"
        );
        Student torill = new Student(
                "Torill Moland Olsen",
                "tol016@uib.no"
        );
        Student torill2 = new Student(
                "Torill Moland Olsen",
                "Torill.Olsen@student.uib.no"
        );
        Student truls = new Student(
                "Truls Paulsen Sletvold",
                "tsl014@uib.no"
        );
        Student truls2 = new Student(
                "Truls Paulsen Sletvold",
                "Truls.Sletvold@student.uib.no"
        );
        Student tora = new Student(
                "Tora Julie Slørdal",
                "tsl010@uib.no"
        );
        Student tora2 = new Student(
                "Tora Julie Slørdal",
                "Tora.Slordal@student.uib.no"
        );

        Student wasihun = new Student(
                "Wasihun Tilahun Abdisa",
                "wab005@uib.no"
        );
        Student wasihun2 = new Student(
                "Wasihun Tilahun Abdisa",
                "Wasihun.Abdisa@student.uib.no"
        );
        Student oygunn = new Student(
                "Øygunn Abelgård Berntsen",
                "obe026@uib.no"
        );
        Student oygunn2 = new Student(
                "Øygunn Abelgård Berntsen",
                "Øygunn.Berntsen@student.uib.no"
        );
        Student xenia = new Student(
                "Xenia Cappelen",
                "xca001@uib.no"
        );
        Student xenia2 = new Student(
                "Xenia Cappelen",
                "Xenia.Cappelen@student.uib.no"
        );
        Student torje = new Student(
                "Torje Olav Uggen",
                "tug007@uib.no"
        );
        Student torje2 = new Student(
                "Torje Olav Uggen",
                "Torje.Uggen@student.uib.no"
        );

        Student vilde = new Student(
                "Vilde Bjarnedatter Moen",
                "vmo013@uib.no"
        );
        Student vilde2 = new Student(
                "Vilde Bjarnedatter Moen",
                "Vilde.Moen@student.uib.no"
        );
        Student trym = new Student(
                "Trym Rauø",
                "tra011@uib.no"
        );
        Student trym2 = new Student(
                "Trym Rauø",
                "Trym.Rauo@student.uib.no"
        );


        Requirement requirement = new Requirement(
                "Sirkulasjon 1",
                "Ultralyd Cor",
                "signature");

     Requirement requirement1 = new Requirement(
             "Sirkulasjon 1",
             "Ultralyd the real deal",
             "signature");
     Requirement requirement2 = new Requirement(
             "Sirkulasjon 1",
             "Ultralyd eksamen",
             "signature");

     Requirement requirement3 = new Requirement(
             "Psykogreie",
             "eksamentest",
             "signature");

     Requirement requirement4 = new Requirement(
             "Psykogreie",
             "eksamentest nummer 2",
             "signature");


        Course MED5 = new Course("MED5");
        MED5.addRequirement(requirement);
        MED5.addRequirement(requirement1);
        MED5.addRequirement(requirement2);
        MED5.addRequirement(requirement3);
        MED5.addRequirement(requirement4);

        courseRepository.save(MED5);
        /*requirementRepository.save(requirement1);
        requirementRepository.save(requirement2);*/


        adrian1.addRequirement(requirement);
        requirement.addLecturer(adrian1);

        adrian2.addRequirement(requirement);
        requirement.addLecturer(adrian2);

        eva.addRequirement(requirement);
        requirement.addLecturer(eva);


        eva2.addRequirement(requirement);
        requirement.addLecturer(eva2);

        Ingeborg.addRequirement(requirement);
        requirement.addLecturer(Ingeborg);

        Hilde.addRequirement(requirement);
        requirement.addLecturer(Hilde);

        Hilde2.addRequirement(requirement);
        requirement.addLecturer(Hilde2);

        adrian3.addRequirement(requirement);
        requirement.addLecturer(adrian3);

        Ingeborg2.addRequirement(requirement);
        requirement.addLecturer(Ingeborg2);

        frankTest.addRequirement(requirement);
        requirement.addLecturer(frankTest);


        lecturerRepository.save(adrian1);
        lecturerRepository.save(adrian2);
        lecturerRepository.save(adrian3);
        lecturerRepository.save(eva);
        lecturerRepository.save(eva2);
        lecturerRepository.save(Ingeborg);
        lecturerRepository.save(Ingeborg2);
        lecturerRepository.save(Hilde);
        lecturerRepository.save(Hilde2);
        lecturerRepository.save(frankTest);

        requirementRepository.save(requirement);



        StudReq studReq = new StudReq();

        //Req1
        studReq.setRequirement(requirement);
        studReq.setStudent(gard);
        studReq.signRequirement(false);
        gard.addStudRequirement(studReq);
        studentRepository.save(gard);


        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(gard2);
        studReq.signRequirement(false);
        gard2.addStudRequirement(studReq);
        studentRepository.save(gard2);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(trond);
        studReq.signRequirement(false);
        trond.addStudRequirement(studReq);
        studentRepository.save(trond);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(trond2);
        studReq.signRequirement(false);
        trond2.addStudRequirement(studReq);
        studentRepository.save(trond2);


        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(torbjorn);
        studReq.signRequirement(false);
        torbjorn.addStudRequirement(studReq);
        studentRepository.save(torbjorn);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(torbjorn2);
        studReq.signRequirement(false);
        torbjorn2.addStudRequirement(studReq);
        studentRepository.save(torbjorn2);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(torill);
        studReq.signRequirement(false);
        torill.addStudRequirement(studReq);
        studentRepository.save(torill);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(torill2);
        studReq.signRequirement(false);
        torill2.addStudRequirement(studReq);
        studentRepository.save(torill2);


        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(tora);
        studReq.signRequirement(false);
        tora.addStudRequirement(studReq);
        studentRepository.save(tora);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(tora2);
        studReq.signRequirement(false);
        tora2.addStudRequirement(studReq);
        studentRepository.save(tora2);


        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(torje);
        studReq.signRequirement(false);
        torje.addStudRequirement(studReq);
        studentRepository.save(torje);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(torje2);
        studReq.signRequirement(false);
        torje2.addStudRequirement(studReq);
        studentRepository.save(torje2);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(oygunn);
        studReq.signRequirement(false);
        oygunn.addStudRequirement(studReq);
        studentRepository.save(oygunn);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(oygunn2);
        studReq.signRequirement(false);
        oygunn2.addStudRequirement(studReq);
        studentRepository.save(oygunn2);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(truls);
        studReq.signRequirement(false);
        truls.addStudRequirement(studReq);
        studentRepository.save(truls);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(truls2);
        studReq.signRequirement(false);
        truls2.addStudRequirement(studReq);
        studentRepository.save(truls2);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(trym);
        studReq.signRequirement(false);
        trym.addStudRequirement(studReq);
        studentRepository.save(trym);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(trym2);
        studReq.signRequirement(false);
        trym2.addStudRequirement(studReq);
        studentRepository.save(trym2);


        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(wasihun);
        studReq.signRequirement(false);
        wasihun.addStudRequirement(studReq);
        studentRepository.save(wasihun);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(wasihun2);
        studReq.signRequirement(false);
        wasihun2.addStudRequirement(studReq);
        studentRepository.save(wasihun2);


        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(vilde);
        studReq.signRequirement(false);
        vilde.addStudRequirement(studReq);
        studentRepository.save(vilde);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(vilde2);
        studReq.signRequirement(false);
        vilde2.addStudRequirement(studReq);
        studentRepository.save(vilde2);


        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(xenia);
        studReq.signRequirement(false);
        xenia.addStudRequirement(studReq);
        studentRepository.save(xenia);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(xenia2);
        studReq.signRequirement(false);
        xenia2.addStudRequirement(studReq);
        studentRepository.save(xenia2);




    }
}

package com.regapp.regapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
    private String errorMessage;

    public BadRequestException( String errorMessage) {
        super(String.format("'%s'", errorMessage));
       this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
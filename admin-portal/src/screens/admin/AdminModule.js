import React, { Component } from 'react'
import { Button, Container, Card, CardHeader, CardBody, CardText, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { getStudentResults } from '../../store/actions';

class AdminModule extends Component {




    render() {

        return (
            <Container className={"App"}>
                <h2 className={"header"}>Administratorverktøy</h2>

                <Row className="row">

                    <Col>
                        <Card>
                            <CardHeader>Last opp</CardHeader>
                            <CardBody>
                                <CardText>Last opp et emne til RegApp.</CardText>

                                <Button href="/admin/upload">Gå til funksjon</Button>

                            </CardBody>

                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <CardHeader>Last ned resultater</CardHeader>
                            <CardBody>
                                <CardText>Resultatene er av filtypen csv.</CardText>
                                <Button onClick={() => this.props.getStudentResults()} > Last ned</Button>
                            </CardBody>

                        </Card>
                    </Col>
                </Row>
                <Row className="row" style={{ marginTop: 20 }}>
                    <Col>
                        <Card>
                            <CardHeader>Felles Studentsystem</CardHeader>
                            <CardBody>
                                <CardText>Hent eller last opp et emne til Felles Studentsystem.</CardText>

                                <Button href="/fs">Gå til funksjon</Button>

                            </CardBody>

                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <CardHeader>Slett emne</CardHeader>
                            <CardBody>
                                <CardText>Slett et emne fra RegApp</CardText>
                                <Button href="/admin/edit">Gå til funksjon</Button>
                            </CardBody>

                        </Card>
                    </Col>
                </Row>
            </Container>)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getStudentResults: () => dispatch(getStudentResults())
    };
};
export default connect(null, mapDispatchToProps)(AdminModule);

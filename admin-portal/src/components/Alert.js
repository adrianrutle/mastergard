import React from 'react'
import { UncontrolledAlert } from 'reactstrap';

const alert = props => (
    props.showAlert ? (<UncontrolledAlert
        color={props.color}>
        {props.alertMessage}
    </UncontrolledAlert>) : null

)
export default alert;
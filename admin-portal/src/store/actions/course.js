import { SET_COURSES, REMOVE_COURSES, SELECTED_COURSE, SELECTED_COURSE_DATA } from "./actionTypes";
import { uiStartLoading, uiStopLoading, showSuccess, showError } from "./ui";
import { responseHandler, fetchAPI, fetchAPIWithRequestBody } from './actionHelper';
import { browserHistory } from '../../App'

export const getCourses = () => {
    return (dispatch, getState) => {

        dispatch(uiStartLoading())
        fetchAPI('/course', 'GET', getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(courses => {
                        dispatch(storeCourses(courses))
                        dispatch(uiStopLoading())
                    })
            })
    }
}

export const updateCourseInfo = courseInfo => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())
        var requestBody = JSON.stringify({
            "title": courseInfo.courseTitle
        })
        fetchAPIWithRequestBody('/course/' + courseInfo.courseId, 'PUT'
            , requestBody, getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then((updatedCourse) => {
                        var coursesInStore = getState().courses.courses
                        coursesInStore.forEach(course => {
                            if (course.id === updatedCourse.id) {
                                course.title = updatedCourse.title
                            }
                        });
                        dispatch(selectCourse(updatedCourse))
                        dispatch(storeCourses(coursesInStore))
                        dispatch(uiStopLoading())
                        dispatch(showSuccess("Emnet er nå oppdatert"))

                    })
            })
    }

}

export const uploadNewCourse = courseInfo => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())

        var requestBody = JSON.stringify({
            "title": courseInfo.courseTitle,
            "students": courseInfo.students,
            "requirements": courseInfo.requirements,
            "lecturers": courseInfo.lecturers,
            "studentRequirements": courseInfo.studentRequirements

        })
        fetchAPIWithRequestBody('/course', 'POST'
            , requestBody, getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                if (response.status === 422) {
                    dispatch(showError("Dette ement finnes!"))

                } else {

                    dispatch(responseHandler(response))
                        .then((newCourse) => {
                            dispatch(uiStopLoading())
                            dispatch(showSuccess("Emnet: " + newCourse.title + " er nå lagt til"))

                        })
                }

            })
    }

}


export const getNumberOfUsersInCourse = () => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())
        fetchAPI('/course/users/' + getState().courses.selectedCourse.id, 'GET', getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(courseData => {
                        dispatch(storeCourseData(courseData))
                        dispatch(uiStopLoading())
                    })
            })
    }
}

export const selectCourse = (course) => {
    return (dispatch) => {
        dispatch({
            type: SELECTED_COURSE,
            course: course
        })
    }
}


export const storeCourseData = (courseData) => {
    return (dispatch) => {
        dispatch({
            type: SELECTED_COURSE_DATA,
            courseData: courseData
        })
    }
}

export const storeCourses = (courses) => {
    return {
        type: SET_COURSES,
        courses: courses
    }
}

export const removeCourses = () => {
    return {
        type: REMOVE_COURSES
    }
}

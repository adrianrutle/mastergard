import {
    SET_COURSES,
    SET_REQUIREMENTS,
    REMOVE_COURSES,
} from "../actions/actionTypes";

const initialState = {
    courses: [],
    requirements: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COURSES:
            return {
                ...state,
                courses: action.courses
            };
        case SET_REQUIREMENTS:
            return {
                ...state,
                requirements: action.requirements
            };
        case REMOVE_COURSES:
            return {
                ...state,
                courses: [],
                requirements: []
            };
        default:
            return state;
    }
};

export default reducer;

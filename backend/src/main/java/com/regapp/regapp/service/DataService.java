package com.regapp.regapp.service;

import com.google.common.io.Files;
import com.regapp.regapp.model.Course;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.repository.CourseRepository;
import com.regapp.regapp.repository.StudReqRepository;
import com.regapp.regapp.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@Service("DataService")
public class DataService {

    @Autowired
    CourseRepository courseRepository;
    @Autowired
    StudentRepository studentRepository;

    /**
     * This function will return all student results
     * @return an array of CSV files
     */
    public List<File> getStudentResultsFiles() {
        List<Course> allCourses = courseRepository.findAll();
        ArrayList<File> files = new ArrayList<>(); 
        for (Course course : allCourses) {
            List<String> fileContent = new ArrayList<>();

            fileContent.add(getStudentResultCSVTableHeader(course.getRequirements()));
            for (Student student : course.getCourseStudents()) {
                fileContent.add(getStudentResultCSVTableRows(student, course.getRequirements()));
            }
            files.add(createTempCSVFile(fileContent,course.getTitle()));
        }
        return files;

    }

    private String getStudentResultCSVTableHeader(List<Requirement> courseRequirements){
        String header = "Student";
        for (Requirement requirement : courseRequirements) {
            header += ", " + requirement.getSubject() + " - " + requirement.getTitle();
        }
        return header;
    }

    private String getRequirementSignedText(StudReq studReq){
        if(studReq.isRequirementSigned()){
            return ", Signert (" + studReq.getSignedTimestamp()+")";
        } else {
            return ", (X)";
        }
    }

    private String getStudentResultCSVTableRows(Student student, List<Requirement> courseRequirements ){
        String tableRow = student.getName();
        for (Requirement requirement : courseRequirements) {
            Boolean isRequirementSigned;
            for (StudReq sr : requirement.getStudReqs()) {
                if(sr.getStudent() == student){
                    isRequirementSigned = sr.isRequirementSigned();
                    tableRow += getRequirementSignedText(sr);
                }
            }
        }
        return tableRow;
    }

    private File createTempCSVFile(List<String> fileContent, String fileName) {
        try {

            File tempFile = new File(Files.createTempDir(), fileName+ ".csv");
            FileWriter writer = new FileWriter(tempFile);
            PrintWriter printWriter = new PrintWriter(writer);
            for (String contentToWrite : fileContent) {
                printWriter.printf(contentToWrite + "\n");

            }
            printWriter.close();
            return tempFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}

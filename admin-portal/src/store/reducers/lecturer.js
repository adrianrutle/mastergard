import {
    SET_COURSE_LECTURERS,
    SET_AVAILABLE_LECTURERS,
    REMOVE_LECTURERS,
    SELECTED_LECTURER
} from "../actions/actionTypes";
const initialState = {
    courseLecturers: [],
    availableLecturers: [],
    selectedLecturer: {}
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COURSE_LECTURERS:
            return {
                ...state,
                courseLecturers: action.lecturers
            };
        case SET_AVAILABLE_LECTURERS:
            return {
                ...state,
                availableLecturers: action.lecturers
            };
        case SELECTED_LECTURER:
            return {
                ...state,
                selectedLecturer: action.lecturer
            };
        case REMOVE_LECTURERS:
            return {
                ...state,
                courseLecturers: [],
                availableLecturers: [],
                selectedLecturer: {}
            };
        default:
            return state;
    }
};

export default reducer;

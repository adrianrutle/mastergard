import React, { Component } from 'react';
import queryString from 'query-string';
import { Button, Container } from 'reactstrap';
import { doAuthorization } from '../store/actions';
import { connect } from 'react-redux';
import Authenticate from 'react-openidconnect';

var OidcSettings = {
    authority: 'https:auth.dataporten.no',
    client_id: '7053f726-7652-47ef-8ece-9991ac315046',
    redirect_uri: 'http://10.111.13.68:8080/oauth-redirect.html',
    // redirect_uri: 'http://localhost:3000/',
    response_type: 'id_token token',
    scope: 'profile groups person eduPersonPrimaryAffiliation eduPersonAffiliation openid userid-feide',
    //post_logout_redirect_uri: 'https://tjenestekort.herokuapp.com/',

    post_logout_redirect_uri: 'http://localhost:3000/'
};
class Auth extends Component {
    componentWillMount() {

        var response = queryString.parse(this.props.location.hash.substr(1))
        var token = response.access_token;
        if (token !== undefined) {
            this.props.doAuthorization(token)
        }
    }
    render() {
        return (<div>
            <Container className="App">
                <h1 className={"header"}>Administrasjonsportal for digitalt tjenestekort</h1>
                <p>Dette er en applikasjon kun tilgjengelig for undervisere og administrasjon</p>

                <div style={{ justifyContent: 'center' }}>
                    <Authenticate OidcSettings={OidcSettings}
                        renderNotAuthenticated={() => (
                            <Button style={{ marginTop: "5%" }}>Logg inn med Feide</Button>
                        )}>

                    </Authenticate>
                </div>

            </Container>
        </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        doAuthorization: (token) => dispatch(doAuthorization(token))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Auth);
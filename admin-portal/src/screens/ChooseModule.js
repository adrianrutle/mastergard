import { Button, Container, Card, CardHeader, CardBody, CardText, Row, Col } from 'reactstrap';

import React, { Component } from 'react'

export default class ChooseModule extends Component {
    render() {
        return (
            <Container className={"App"}>
                <h2 className={"header"}>Hva ønsker du å gjøre?</h2>

                <Row className="row" style={{ marginTop: 50 }}>

                    <Col>
                        <Card>
                            <CardHeader>Administratorverktøy (Admin)</CardHeader>
                            <CardBody>

                                <CardText>Administrere Felles Studentsystem.</CardText>
                                <CardText>Last opp et emne til RegApp.</CardText>
                                <CardText>Last ned resultater.</CardText>
                                <CardText>Slett emne.</CardText>
                                <Button href="/admin/module">Gå til modul</Button>

                            </CardBody>

                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <CardHeader>Emner</CardHeader>
                            <CardBody>
                                <CardText>Se statistikk.</CardText>
                                <CardText>Send qr-kode til foreleser.</CardText>
                                <CardText>Oppdater emneinformasjon.</CardText>

                                <CardText>Administrere forelesere.</CardText>

                                <Button href="/courses">Gå til Modul</Button>
                            </CardBody>

                        </Card>
                    </Col>
                </Row>

            </Container>)
    }
}

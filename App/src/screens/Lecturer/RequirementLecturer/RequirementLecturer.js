import React, { Component } from 'react';
import { StyleSheet, View, Text, FlatList, ScrollView, RefreshControl } from 'react-native';
import { connect } from "react-redux";
import { List } from "react-native-elements";
import Panel from '../../../components/UI/Panel/Panel';
import HeadingText from '../../../components/UI/HeadingText/HeadingText'
import { getCourseLecturerRequirements } from '../../../store/actions';
import ButtonWithIcon from '../../../components/UI/Button/ButtonWithIcon';
import AlertHandler from '../../../components/AlertHandler/AlertHandler';

class RequirementLecturerScreen extends Component {
    componentDidMount() {
        this.props.getRequirements(this.props.courseId)
    }

    _onRefresh = () => {
        this.props.getRequirements(this.props.courseId)

    }

    navigateToQRScreen = (requirementId, requirementTitle) => {

        this.props.navigator.push({
            passProps: {
                requirementId: requirementId,
                requirementTitle: requirementTitle
            },

            screen: "regapp.GenerateQRScreen",
            title: "QR-kode",

        });
    }
    renderRequirements = () => {
        if (!this.props.isLoading) {
            return this.requirements()
        }
    }

    requirements = () => {
        var content = [];
        this.props.requirements.forEach(requirement => {
            content.push(
                <Panel
                    panelHeaderSize={50}
                    panelContainerSize={50}
                    key={requirement.subject}
                    title={requirement.subject}
                    numberOfTasks={requirement.tasks.length}>
                    <List containerStyle={{
                        borderTopWidth: 0,
                        borderBottomWidth: 0
                    }} >
                        <FlatList
                            data={requirement.tasks}
                            renderItem={({ item }) => (

                                <ButtonWithIcon

                                    icon={"ios-arrow-forward"}
                                    iconColor={"#48acb8"}
                                    iconSize={25}
                                    onPress={() => this.navigateToQRScreen(item.id, item.title)}>
                                    <Text>{item.title}</Text>
                                </ButtonWithIcon>
                            )}
                            keyExtractor={item => item.id.toString()}

                        />
                    </ List>

                </Panel >
            )

        });
        return content;
    }

    render() {

        return (
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={this.props.isLoading}
                        onRefresh={this._onRefresh}
                    />}
                style={styles.container} >
                <AlertHandler />
                <HeadingText style={{ marginBottom: '6%' }}>Obligatoriske aktiviteter</HeadingText>
                <View style={styles.requirementContainer}>
                    {this.renderRequirements()}
                </View>

            </ScrollView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#edf0f5',
    },
    requirementContainer: {
        flex: 1,
    },

});

const mapStateToProps = state => {
    return {
        requirements: state.courses.requirements,
        isLoading: state.ui.isLoading

    };
};

const mapDispatchToProps = dispatch => {
    return {
        getRequirements: (courseId) => dispatch(getCourseLecturerRequirements(courseId)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RequirementLecturerScreen);
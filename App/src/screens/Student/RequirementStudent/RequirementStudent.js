import React, { Component } from 'react';
import { ScrollView, View, Text, StyleSheet, FlatList, RefreshControl } from 'react-native';
import { connect } from "react-redux";
import { List } from 'react-native-elements'
import { getCourseRequirements } from '../../../store/actions';
import Panel from '../../../components/UI/Panel/Panel';
import HeadingText from '../../../components/UI/HeadingText/HeadingText'
import ButtonWithIcon from '../../../components/UI/Button/ButtonWithIcon';


class RequirementStudentScreen extends Component {
    componentDidMount() {
        this.props.getRequirements(this.props.courseId)
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#CED0CE",

                }}
            />
        );
    };

    navigateToScanScreen = (requirementId, requirementTitle) => {

        this.props.navigator.push({
            passProps: {
                requirementId: requirementId,
                requirementTitle: requirementTitle
            },
            screen: "regapp.ScanQR",
            title: requirementTitle,

        });
    }

    _onRefresh = () => {
        this.props.getRequirements(this.props.courseId)
    }

    renderRequirements = () => {
        if (!this.props.isLoading) {
            return this.requirements()
        }
    }
    requirements = () => {
        var content = [];
        this.props.requirements.forEach(requirement => {
            console.log(requirement);

            content.push(
                <Panel
                    panelHeaderSize={50}
                    panelContainerSize={85}
                    key={requirement.subject}
                    title={requirement.subject}
                    requirementCount={requirement.numberOfSignedTasks + ' / ' + requirement.tasks.length}
                    numberOfTasks={requirement.tasks.length}>
                    <List containerStyle={{
                        borderTopWidth: 0,
                        borderBottomWidth: 0
                    }} >
                        <FlatList
                            data={requirement.tasks}
                            renderItem={({ item }) => (
                                <View style={{ marginTop: 2 }}>
                                    <View style={{ marginBottom: 1 }} >
                                        <Text>Dato: {item.date}</Text>
                                        <Text>Gruppe: {item.studentGroup}</Text>
                                    </View>
                                    <ButtonWithIcon

                                        style={item.signed ?
                                            { backgroundColor: '#58eaa1' }
                                            : undefined}
                                        icon={item.signed ?
                                            'ios-checkmark-circle-outline' : 'ios-arrow-forward'}
                                        iconColor={item.signed ?
                                            '#eeeeee' : '#48acb8'}

                                        iconSize={item.signed ?
                                            35 : 25}
                                        onPress={() => item.signed ? undefined : this.navigateToScanScreen(item.id, item.title)}
                                    >
                                        <Text>{item.title}</Text>
                                    </ButtonWithIcon>
                                </View>


                            )}
                            keyExtractor={item => item.id.toString()}

                        />
                    </ List>

                </Panel >
            )

        });
        return content;
    }


    render() {

        return (
            <ScrollView
                refreshControl={
                    <RefreshControl
                        tintColor={"#80deea"}
                        refreshing={this.props.isLoading}
                        onRefresh={this._onRefresh}
                    />}
                style={styles.container} >
                <HeadingText style={{ marginBottom: '6%' }}>Obligatoriske aktiviteter</HeadingText>
                {this.renderRequirements()}

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#edf0f5',
    }
});

const mapStateToProps = state => {
    return {
        requirements: state.courses.requirements,
        isLoading: state.ui.isLoading

    };
};

const mapDispatchToProps = dispatch => {
    return {
        getRequirements: (courseId) => dispatch(getCourseRequirements(courseId)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(RequirementStudentScreen);
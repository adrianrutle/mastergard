import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Container, Card, CardHeader, CardBody, CardText, Row, Col } from 'reactstrap';

import { browserHistory } from '../App'
import LoadingSpinner from '../components/LoadingSpinner';

import { getNumberOfUsersInCourse, getRequirementForCourse } from '../store/actions'
import ColumnChart from '../components/Charts/ColumnChart';
import DrilldownChart from '../components/Charts/DrilldownChart';
export class CourseDashboard extends Component {

    componentDidMount() {
        this.props.getNumberOfUsersInCourse();
        this.props.getRequirementForCourse();
    }

    routeToPage(route) {
        browserHistory.push(route)
    }
    render() {

        if (this.props.selectedCourse.courseData === undefined ||
            this.props.requirements === undefined ||
            this.props.isLoading === true) {
            return (<LoadingSpinner />)
        }
        return (
            <Container className={"App"}>
                <h1 className={"header"}>{this.props.selectedCourse.title}</h1>
                <Row className="row">

                    <Col>

                        <DrilldownChart data={this.props.requirements}></DrilldownChart>
                    </Col>
                    <Col>
                        <ColumnChart data={this.props.selectedCourse} />

                    </Col>
                </Row>
                <Row className="row">
                    {/*  <Col>
                        <Card>
                            <CardHeader>Emner og aktiviteter</CardHeader>
                            <CardBody>
                                <CardText>Redigere informasjon i et emne eller en aktiviet.</CardText>
                                <Button onClick={() => this.routeToPage("/course/update")}>Rediger emne</Button>
                                <Button onClick={() => this.routeToPage("/lecturers")}>Undervisere</Button>

                                <Button onClick={() => this.routeToPage("/course/requirements")}>Aktiviteter</Button>

                            </CardBody>

                        </Card>
                    </Col> */}


                    <Col>
                        <Card>
                            <CardHeader>Aktiviteter</CardHeader>
                            <CardBody>
                                <CardText>Se status fra emnets aktiviteter</CardText>


                                <Button href={'/course/requirements'}>Gå til funksjon</Button>

                            </CardBody>

                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <CardHeader>Send QR-kode</CardHeader>
                            <CardBody>
                                <CardText>Send QR-kode til foreleser </CardText>

                                <Button >Gå til funksjon</Button>

                            </CardBody>

                        </Card>
                    </Col>

                </Row>

                <Row className="row" style={{ marginTop: 20 }}>
                    <Col>
                        <Card>
                            <CardHeader>Forelesere (Admin)</CardHeader>
                            <CardBody>
                                <CardText>Rediger forelesere som skal ha tilgang til emnet</CardText>


                                <Button href={'/lecturers'}>Gå til funksjon</Button>

                            </CardBody>

                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <CardHeader>Rediger emne (Admin)</CardHeader>
                            <CardBody>
                                <CardText>Rediger emneinformasjon</CardText>

                                <Button href={'/course/update'} >Gå til funksjon</Button>

                            </CardBody>

                        </Card>
                    </Col>

                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => ({
    selectedCourse: state.courses.selectedCourse,
    requirements: state.requirements.requirements,
    isLoading: state.ui.isLoading
})

const mapDispatchToProps = (dispatch) => {
    return {
        getNumberOfUsersInCourse: () => dispatch(getNumberOfUsersInCourse()),
        getRequirementForCourse: () => dispatch(getRequirementForCourse())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseDashboard)

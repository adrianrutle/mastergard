import React from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import HC_drilldown from 'highcharts/modules/drilldown'

HC_drilldown(Highcharts)

export const drilldown = (requirements) => {
    var sortedRequirements = []
    var allRequirementTitles = [];
    requirements.forEach(requirement => {
        if (!allRequirementTitles.includes(requirement.title)) {
            allRequirementTitles.push(requirement.title)
        }
        if (sortedRequirements.some(sortedReq => sortedReq.subject === requirement.subject)) {
            sortedRequirements.forEach(sortedReq => {
                if (!sortedReq.titles.includes(requirement.title)) {
                    sortedReq.titles.push(requirement.title)
                }
            });
        } else {
            sortedRequirements.push({
                subject: requirement.subject,
                titles: [requirement.title]
            })
        }
    });
    return {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aktiviteter'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Antall aktiviteter'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        tooltip: {
            enabled: true
        },
        credits: {
            enabled: false
        },

        series: [{
            "name": "Antall aktiviteter",
            "colorByPoint": true,
            data: [
                {
                    "name": "Aktiviteter",
                    "y": allRequirementTitles.length + 28,
                    "drilldown": "Aktiviteter"
                }]
        }],
        "drilldown": {
            "series": [{
                "name": "Aktiviteter",
                "colorByPoint": true,
                "id": "Aktiviteter",
                "data": (function () {
                    var data = []
                    sortedRequirements.forEach(requirement => {
                        data.push([
                            requirement.subject,
                            requirement.titles.length
                        ])
                    });
                    data.push([
                        "Nevrologi",
                        7

                    ])
                    data.push([
                        "Øye",
                        8
                    ])
                    data.push([
                        "ØNH",
                        13
                    ])
                    return data;
                }())
            }
            ]
        }
    }
}

const DrilldownChart = (props) => {
    return (
        <div>
            <HighchartsReact
                highcharts={Highcharts}
                options={drilldown(props.data)}
            />
        </div>
    )
}
export default DrilldownChart;

import { AUTHORIZE, LOGOUT } from "../actions/actionTypes";

const initialState = {
    isAuthenticated: false,
    token: ""
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTHORIZE:
            return {
                ...state,
                isAuthenticated: true,
                token: action.token
            };

        case LOGOUT:
            return {
                ...state,
                isAuthenticated: false,
                token: ''
            };
        default:
            return state;
    }
};
export default reducer;
package com.regapp.regapp.repository;

import com.regapp.regapp.model.jointable.StudReq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudReqRepository extends JpaRepository<StudReq, Long> {
}

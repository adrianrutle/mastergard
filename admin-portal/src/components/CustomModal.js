import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class CustomModal extends Component {
    state = {
        modal: false
    }
    toggleModal() {
        this.setState({
            modal: !this.state.modal
        });
    }
    render() {
        return (
            <div>
                <Modal isOpen={this.state.modal} toggle={() => this.toggleModal()} className={this.props.className}>
                    <ModalHeader toggle={() => this.toggleModal()}>{this.props.title}</ModalHeader>
                    <ModalBody>
                        {this.props.body}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={() => this.props.posButtonFunc()}>{this.props.posButton}</Button>{' '}
                        <Button color="secondary" onClick={() => this.props.negButtonFunc()}>{this.props.negButton}</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

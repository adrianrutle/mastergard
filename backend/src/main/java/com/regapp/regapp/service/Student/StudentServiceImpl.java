package com.regapp.regapp.service.Student;

import com.regapp.regapp.exception.ResourceNotFoundException;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("StudentService")
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student setFeideIdToStudent(String feideId){
        Student student = getLoggedInStudent();
        if(student.getUserid() == null){
            student.setUserid(feideId);
            return studentRepository.save(student);
        } else {
            return student;
        }

    }
    @Override
    public Boolean checkIfUserIsStudent(String email){
        Student studentByEmail = studentRepository.findStudentByEmail(email);
        if(studentByEmail != null){
            return true;
        } else {
            return false;
        }
    }
    @Override
    public Student getStudentById(Long studentId) {
        return studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student", "id", studentId));
    }

    @Override
    public Student getStudentByUserid(String userid) {
        System.out.println(userid);
        if (studentRepository.findStudentByUserid(userid) != null) {
            return studentRepository.findStudentByUserid(userid);
        } else {
            throw new ResourceNotFoundException("Student", "userid", userid);
        }
    }

    @Override
    public List<Requirement> getAllRequirementsForStudent(Long studentId) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student", "studentId", studentId));
        return student.getAllRequirements();
    }

    @Override
    public Boolean isRequirementSignedForStudent(Long studentId, Long requirementId) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student", "studentId", studentId));
        List<StudReq> studReqs = student.getStudReqs();
        for (StudReq sr : studReqs) {
            Requirement r = sr.getRequirement();
            if (r.getId().equals(requirementId)) {
                return sr.isRequirementSigned();
            }
        }
        throw new ResourceNotFoundException("Requirement", "requirementId", requirementId);
    }

    @Override
    public Student updateStudent(Long studentId, Student studentDetails) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student", "studentId", studentId));
        student.setName(studentDetails.getName());
        student.setEmail(studentDetails.getEmail());
        Student updatedStudent = studentRepository.save(student);
        return updatedStudent;
    }

    @Override
    public void deleteStudent(Long studentId) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student", "studentId", studentId));
        studentRepository.delete(student);
    }

    private Student getLoggedInStudent(){
        OAuth2Authentication principal =  (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        Object object=principal.getUserAuthentication().getDetails();
        Map<String, Object> studentDetails = (Map<String, Object>) object;
        String email = studentDetails.get("eduPersonPrincipalName").toString();
        return studentRepository.findStudentByEmail(email);
    }
}

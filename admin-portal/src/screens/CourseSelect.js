import React, { Component } from 'react';
import { Button, Container, Table, UncontrolledButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap';
import { connect } from 'react-redux';
import { getCourses, selectCourse } from '../store/actions';
import { browserHistory } from '../App'
import LoadingSpinner from '../components/LoadingSpinner';

class CourseSelect extends Component {

    componentDidMount() {
        if (!this.props.courses.length > 0) {
            this.props.getCourses()
        }
    }

    onCourseSelect(course) {
        this.props.selectCourse(course)
        browserHistory.push('/dashboard')
    }

    populateCourseTable() {
        if (this.props.courses.length > 0) {
            return this.props.courses.map(course => {
                return <tr key={course.id}>
                    <td style={{ whiteSpace: 'nowrap' }}>{course.title}</td>

                    <td>
                        <Button onClick={() => this.onCourseSelect(course)}>Velg</Button>
                    </td>
                </tr>
            });
        }

    }
    render() {
        if (this.props.isLoading) {
            return (<LoadingSpinner />)
        }
        return (
            <div>
                <Container className={"App"}>
                    <h2 className={"header"}>Velg Emne</h2>

                    <div className={"dropdown"}>


                        <UncontrolledButtonDropdown>
                            <DropdownToggle caret>
                                Kull 2019
                         </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem>Kull 2019</DropdownItem>
                                <DropdownItem>Kull 2018</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    </div>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th width="20%">Emne</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.populateCourseTable()}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        courses: state.courses.courses,
        isLoading: state.ui.isLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCourses: () => dispatch(getCourses()),
        selectCourse: (course) => dispatch(selectCourse(course))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(CourseSelect);
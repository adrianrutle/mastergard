import {
    SHOW_ERROR, UI_START_LOADING,
    UI_STOP_LOADING, SHOW_INFO, HIDE_ALERT, SHOW_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    isLoading: false,
    alertMessage: "",
    alertColor: "info",
    showAlert: false
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_ERROR:
            return {
                ...state,
                showAlert: true,
                alertColor: "danger",
                alertMessage: action.alertMessage
            };

        case SHOW_INFO:
            return {
                ...state,
                showAlert: true,
                alertColor: "info",
                alertMessage: action.alertMessage
            };

        case SHOW_SUCCESS:
            return {
                ...state,
                showAlert: true,
                alertColor: "success",
                alertMessage: action.alertMessage
            };

        case HIDE_ALERT:
            return {
                ...state,
                showAlert: false,
                alertColor: "info",
                alertMessage: ""
            };




        case UI_START_LOADING:
            return {
                ...state,
                isLoading: true
            };

        case UI_STOP_LOADING:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
};
export default reducer;
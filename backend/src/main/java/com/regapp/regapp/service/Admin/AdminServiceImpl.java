package com.regapp.regapp.service.Admin;

import com.regapp.regapp.model.Admin;
import com.regapp.regapp.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("AdminService")
public class AdminServiceImpl implements AdminService{
    @Autowired
    AdminRepository adminRepository;
    @Override
    public Boolean checkIfUserIsAdmin(String email) {
        Admin adminByEmail = adminRepository.findAdminByEmail(email);
        if(adminByEmail != null){
            return true;
        } else {
            return false;
        }
    }
}

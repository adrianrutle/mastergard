import { SET_COURSE_LECTURERS, SET_AVAILABLE_LECTURERS, REMOVE_LECTURERS, SELECTED_LECTURER } from "./actionTypes";
import { uiStartLoading, uiStopLoading, showSuccess } from "./ui";
import { responseHandler, fetchAPI, fetchAPIWithRequestBody } from './actionHelper';


export const getCourseLecturers = (courseId) => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())
        return fetchAPI('/course/lecturer/' + courseId, 'GET', getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(courseLecturers => {
                        dispatch(storeCourseLecturers(courseLecturers))
                        dispatch(getAvailableLecturers(courseLecturers))
                    })
            })
    }
}

export const getAvailableLecturers = () => {
    return (dispatch, getState) => {
        return fetchAPI('/lecturer/', 'GET', getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(availableLecturers => {
                        let courseLecturers = getState().lecturer.courseLecturers;
                        let lecturers = filterLecturers(courseLecturers, availableLecturers)
                        dispatch(storeAvailableLecturers(lecturers))
                        dispatch(uiStopLoading())
                    })
            })
    }
}

const filterLecturers = (courseLecturers, availableLecturers) => {
    var updatedAvailableLecturers = []
    availableLecturers.forEach(avLec => {
        var isLecturerAvailable = true;
        courseLecturers.forEach(coLec => {
            if (avLec.id === coLec.id) {
                isLecturerAvailable = false;
            }
        });
        if (isLecturerAvailable) {
            updatedAvailableLecturers.push(avLec)
        }
    });
    return updatedAvailableLecturers;
}



/* export const addLecturerToCourse = (lecturerId) => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())
        fetchAPI('/lecturer/', 'GET', getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(availableLecturers => {
                        var filteredAvailableLecturers =
                            filterLecturers(
                                getState().lecturer.courseLecturers,
                                availableLecturers)
                        dispatch(storeAvailableLecturers(filteredAvailableLecturers))
                        dispatch(uiStopLoading())
                    })
            })
    }
} */



export const removeLecturerFromCourse = lecturerId => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())
        fetchAPI('/course/lecturer/remove/' +
            lecturerId + "/" +
            getState().courses.selectedCourse.id,
            'PUT',
            getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(() => {
                        let courseLecturers = getState().lecturer.courseLecturers
                        let availableLecturers = getState().lecturer.availableLecturers

                        let updatedCourseLecturers = courseLecturers
                        courseLecturers.forEach((courseLec, i) => {
                            if (courseLec.id === lecturerId) {
                                updatedCourseLecturers.splice(i, 1)
                                availableLecturers.push(courseLec)
                            }
                        })
                        dispatch(storeCourseLecturers(updatedCourseLecturers))
                        dispatch(storeAvailableLecturers(availableLecturers))

                        dispatch(showSuccess("Underviseren er fjernet."))
                        dispatch(uiStopLoading())
                    })
            })
    }
}






export const selectLecturer = (lecturer) => {
    return (dispatch) => {
        dispatch({
            type: SELECTED_LECTURER,
            lecturer: lecturer
        })
    }
}



export const storeAvailableLecturers = (availableLecturers) => {
    return {
        type: SET_AVAILABLE_LECTURERS,
        lecturers: availableLecturers
    }
}

export const storeCourseLecturers = (courseLecturers) => {
    return {
        type: SET_COURSE_LECTURERS,
        lecturers: courseLecturers
    }
}
export const removeLecturers = () => {
    return {
        type: REMOVE_LECTURERS
    }
}

import { showError, showInfo } from "./ui";
import { removeCourses } from "./course";
import { removeLecturers } from "./lecturer";
import { removeRequirements } from "./requirement";
import { browserHistory } from '../../App'
import { AUTHORIZE, LOGOUT } from './actionTypes'

export const doAuthorization = (token) => {
    return (dispatch) => {
        fetch('/role', {
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + token
            }
        })
            .catch(err => {
                console.log(err);
            })
            .then((response) => response.json())
            .then(response => {
                if (response.status === 404) {
                    browserHistory.replace('/')
                    dispatch(showError("Du har ikke tilgang til administrasjonsportalen"))
                } else if (response[0].authority === "ROLE_LECTURER" || response[0].authority === "ROLE_ADMIN") {
                    dispatch(authorize(token))
                    browserHistory.replace('/modules')
                } else {
                    browserHistory.replace('/')
                    dispatch(showError("Du har ikke tilgang til administrasjonsportalen"))
                }
            })
    }
}
export const doLogout = () => {
    return (dispatch) => {
        browserHistory.replace('/')
        dispatch(logout())
        dispatch(removeCourses())
        dispatch(removeLecturers())
        dispatch(removeRequirements())
        dispatch(showInfo("Du er nå logget ut av administrasjonssystemet"))
    }
}
export const logout = () => {
    return {
        type: LOGOUT
    }
}

const authorize = token => {
    return {
        type: AUTHORIZE,
        token: token
    }
}







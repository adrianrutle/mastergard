import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoadingSpinner from '../components/LoadingSpinner';
import { getRequirementForCourse, selectRequirement } from '../store/actions'
import { Button, Container, Table, Input } from 'reactstrap';
import { browserHistory } from '../App'



export class CourseRequirements extends Component {
    state = {
        requirementSearch: '',
        requirements: [],
        filteredRequirements: [],
    }

    componentDidMount() {
        if (this.props.requirements === undefined) {
            this.props.getRequirementForCourse()
        } else {
            this.setState({
                requirements: this.props.requirements,
                filteredRequirements: this.props.requirements,
            })
        }
    }
    componentWillReceiveProps() {
        if (this.props.requirements === undefined) {
            this.setState({
                requirements: this.props.requirements,
                filteredRequirements: this.props.requirements,
            })
        }
    }

    goToRequirement(requirement) {

        this.props.selectRequirement(requirement)
        browserHistory.push('/requirement')
    }

    populateRequirementTable(requirementData) {
        if (requirementData.length > 0) {
            return requirementData.map(requirement => {
                return <tr key={requirement.id}>
                    <td style={{ whiteSpace: 'nowrap' }}>{requirement.title}</td>
                    <td style={{ whiteSpace: 'nowrap' }}>{requirement.subject}</td>
                    <td>
                        <Button
                            color="info"
                            onClick={() => this.goToRequirement(requirement)}>
                            Velg
              </Button>
                    </td>
                </tr>
            });
        }
    }

    filterTable(value) {
        let filteredRequirements = this.state.requirements
        filteredRequirements = filteredRequirements.filter((requirement) => {
            let requirementTitle = requirement.title.toLowerCase()
            return requirementTitle.indexOf(value.toLowerCase()) !== -1
        })
        this.setState({
            filteredRequirements: filteredRequirements
        })
    }

    requirementTable() {
        if (this.props.requirements !== undefined) {
            return (
                <div>
                    <h2 className={"header"}>Aktiviteter i {this.props.selectedCourse.title}</h2>
                    <Input type="email"
                        name="email"
                        id="exampleEmail"
                        onChange={(e) => this.filterTable(e.target.value)}
                        placeholder="Søk etter en aktivitet" />

                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th width="50%">Aktivitet</th>
                                <th width="50%">Fagområde</th>

                            </tr>
                        </thead>
                        <tbody>
                            {this.populateRequirementTable(this.state.filteredRequirements)}
                        </tbody>
                    </Table>
                </div>
            )
        } else {
            return (
                <div>
                    <h2>Ingen aktiviteter tilgjengelig</h2>
                </div>
            )
        }
    }



    render() {
        if (this.props.isLoading) {
            return (<LoadingSpinner />)
        }
        return (
            <div>
                <Container className={"App"}>
                    {this.requirementTable()}
                </Container>
            </div>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        selectedCourse: state.courses.selectedCourse,
        requirements: state.requirements.requirements,
        isLoading: state.ui.isLoading

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getRequirementForCourse: () => dispatch(getRequirementForCourse()),
        selectRequirement: (requirement) => dispatch(selectRequirement(requirement)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseRequirements)


package com.regapp.regapp.config;

import com.regapp.regapp.service.Admin.AdminService;
import com.regapp.regapp.service.Lecturer.LecturerService;
import com.regapp.regapp.service.Student.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class FeideAuthoritiesExtractor implements AuthoritiesExtractor {
    @Autowired
    public StudentService studentService;

    @Autowired
    public LecturerService lecturerService;

    @Autowired
    public AdminService adminService;

    @Override
    public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {
        /*if(map.get("o") == null){
            System.out.println("Not part of UiB");
            return Collections.<GrantedAuthority>emptyList();
        }*/
      //  String university = map.get("o").toString().toLowerCase();
        String university = "universitetet i bergen";
        String email = map.get("eduPersonPrincipalName").toString().toLowerCase();
        System.err.println("***** RegApp detected user with email: " + email);
            if (university.equals("universitetet i bergen")) {
                if (studentService.checkIfUserIsStudent(email)) {
                    // if (primaryAffiliation.toLowerCase().equals("student")) {
                    return AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_STUDENT");
                    // }
                }
                if (lecturerService.checkIfUserIsLecturer(email)) {
                    // if (primaryAffiliation.toLowerCase().equals("employee")) {
                    return AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_LECTURER");
                    // }
                }
                if (adminService.checkIfUserIsAdmin(email)) {
                    return AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN");
                }
                return Collections.<GrantedAuthority>emptyList();
                }
        return Collections.<GrantedAuthority>emptyList();
    }
}

package com.regapp.regapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MissingParametersException extends RuntimeException{
    public MissingParametersException() {
        super("Missing parameters.");
    }
}

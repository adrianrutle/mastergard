package com.regapp.regapp.service.Requirement;

import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import org.json.JSONArray;

import java.util.List;

public interface RequirementService {
    List<Requirement> getAllRequirements();
    List<Student> getAllRelatedStudents(Long requirementId);
    String getRequirementData(Long requirementId);
}

import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, Image, Platform, ScrollView } from 'react-native';
import { List, ListItem } from "react-native-elements";
import { connect } from "react-redux";
import { getAllCourses } from "../../../store/actions";
import MainText from '../../../components/UI/MainText/MainText';
import IconIonic from "react-native-vector-icons/Ionicons"
import AlertHandler from '../../../components/AlertHandler/AlertHandler';

class HomeLecturer extends Component {

    state = {
        name: null,
        email: null
    }
    componentDidMount() {
        this.props.getAllCourses();
        this.state.name = this.props.lecturer.name,
            this.state.email = this.props.lecturer.email
    }



    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "12%"
                }}
            />
        );
    };

    renderHeader = () => {
        return <Text style={styles.headerText}>Dine emne:</Text>;
    };

    navigateToRequirement = (courseId, courseTitle) => {
        this.props.navigator.push({
            passProps: {
                courseId: courseId,
                courseTitle: courseTitle
            },
            screen: "regapp.RequirementLecturerScreen",
            title: courseTitle,

        });
    }
    render() {
        return (
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <AlertHandler />
                <View style={styles.container}>
                    <View style={styles.profileContainer}>
                        <View style={styles.image}>
                            <Image
                                style={{ width: 120, height: 120 }}
                                source={require('../../../assets/mock-user-image.png')
                                }
                            />
                        </View>
                        <View style={styles.profileInfo}>
                            <MainText style={{ fontSize: 30 }}>{this.state.name}</MainText>
                            <MainText style={{ fontSize: 20 }}>Underviser</MainText>
                            <MainText>email: {this.state.email}</MainText>
                            <MainText>Universitetet i Bergen</MainText>
                        </View>
                    </View>
                    <List containerStyle={styles.courseContainer} >
                        <FlatList
                            data={this.props.courses}
                            renderItem={({ item }) => (
                                <ListItem
                                    containerStyle={styles.course}
                                    roundAvatar
                                    title={item.title}
                                    underlayColor={'white'}
                                    onPress={() => { this.navigateToRequirement(item.id, item.title) }}

                                    rightIcon={<IconIonic color={'#48acb8'}
                                        name={'ios-arrow-forward'}
                                        size={25} />}
                                    avatar={<IconIonic color={'#48acb8'} name={Platform.OS === 'android' ? "md-school" : "ios-school"} size={40} />}

                                />
                            )}

                            keyExtractor={item => item.id.toString()}
                            ListHeaderComponent={this.renderHeader}
                        //ItemSeparatorComponent={this.renderSeparator}
                        />
                    </ List>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#b4ffff'
    },
    profileContainer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    courseContainer: {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        flex: 3,
        backgroundColor: '#edf0f5'
    },
    course: {
        borderBottomWidth: 0,
        backgroundColor: '#fff',
        marginTop: 5,
        borderRadius: 20
    },
    headerText: {
        marginTop: '5%',
        marginBottom: 5,
        paddingLeft: 10,
        fontSize: 20
    }
});
const mapStateToProps = state => {
    return {
        courses: state.courses.courses,
        lecturer: state.user.lecturer
    };
};
const mapDispatchToProps = dispatch => {
    return {
        getAllCourses: () => dispatch(getAllCourses()),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeLecturer);
package com.regapp.regapp.controller;

import com.regapp.regapp.model.Lecturer;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.service.Lecturer.LecturerService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@RestController
public class LecturerController {
    @Autowired
    LecturerService lecturerService;

    @GetMapping("/lecturer")
    public List<Lecturer> getLecturers() {
        return lecturerService.getLecturers();
    }

    @PostMapping("/lecturer/feide/{feideId}")
    public Lecturer setFeideIdToLecturer(@PathVariable(value = "feideId") String feideId) {
        return lecturerService.setFeideIdToLecturer(feideId);
    }

    @GetMapping("/lecturer/requirement/signature/{lecturerId}/{requirementId}")
    public String getQRString(
            @PathVariable(value = "lecturerId") Long lecturerId,
            @PathVariable(value = "requirementId") Long requirementId) {
        return lecturerService.getQRString(lecturerId,requirementId);
    }

    //TODO rename this endpoint
   @PutMapping("/lecturer/requirement/sign")
    public StudReq signRequirement(HttpEntity<String> httpEntity) {
        JSONObject requestBody = new JSONObject(httpEntity.getBody());
       Long studentId = Long.valueOf(requestBody.get("studentId").toString());
       String qrString = requestBody.get("qrString").toString();
       Long requirementId = Long.valueOf(requestBody.get("requirementId").toString());

       return lecturerService.signRequirement(studentId, qrString, requirementId);
    }

    /**
     * Usage
     */
    @PostMapping("/lecturer/requirement/{lecturerId}")
    public Lecturer addCourseRequirementsToLecturer(@PathVariable(value = "lecturerId") Long lecturerId, @Valid @RequestBody List<Long> requirementIds) {
        return lecturerService.addCourseRequirementsToLecturer(lecturerId, requirementIds);
    }
}

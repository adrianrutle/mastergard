import { AUTH_SET_TOKEN, AUTH_REMOVE_TOKEN } from "./actionTypes"
import { storeItemInStorage, getItemFromStorage, clearStorage } from "../util"

export const getToken = () => {
    return (dispatch, getState) => {
        const promise = new Promise((resolve, reject) => {
            const token = getState().auth.token;
            if (!token) { // if token is missing in redux, check local storage
                console.log(getItemFromStorage('test:auth:token'));
                getItemFromStorage('test:auth:token')

                    .catch(err => {
                        console.log('errrr:' + err);
                        reject()
                    })
                    .then(tokenFromStorage => {

                        if (tokenFromStorage === null) {
                            reject();
                        } else {
                            dispatch(setToken(tokenFromStorage));
                            resolve(tokenFromStorage);
                        }
                    });
            } else {
                resolve(token);
            }

        });
        promise.catch(err => {
            console.log('err:' + err);
            dispatch(clearStorage());
        });
        return promise;
    };
};

export const storeToken = token => {
    return dispatch => {
        dispatch(setToken(token));
        storeItemInStorage('test:auth:token', token)
    }
}
/**
 * Remove access token form Redux
 */
export const removeToken = () => {
    return {
        type: AUTH_REMOVE_TOKEN
    }
}

/**
 * This function will store the access token of the logged in user in Redux 
 * @param {The access token of the logged in user} token 
 */
export const setToken = token => {
    return {
        type: AUTH_SET_TOKEN,
        token: token
    };
};
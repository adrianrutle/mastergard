package com.regapp.regapp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.regapp.regapp.model.jointable.StudReq;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "REQUIREMENTS")
public class Requirement {

    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String title;
    @NotBlank
    private String subject;
    @NotBlank
    private String signature;

    @OneToMany(mappedBy = "primaryKey.requirement", cascade = {CascadeType.REMOVE})
    @JsonIgnore
    private List<StudReq> studReqs = new ArrayList<>();

    @ManyToMany(mappedBy = "requirements")
    @JsonIgnore
    private List<Lecturer> lecturers = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    @JsonBackReference
    private Course course;

    public Requirement(@NotBlank String subject,@NotBlank String title, @NotBlank String signature) {
        this.title = title;
        this.subject = subject;
        this.signature = signature;
    }

    public Requirement() {
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Long getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonIgnore
    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public List<StudReq> getStudReqs() {
        return studReqs;
    }

    public void setStudReqs(List<StudReq> studReqs) {
        this.studReqs = studReqs;
    }

    @JsonIgnore
    public List<Student> getAllStudents() {
        List<Student> students = new ArrayList<>();
        for (StudReq sr : getStudReqs()) {
            students.add(sr.getStudent());
        }
        return students;
    }

    @JsonIgnore
    public List<Lecturer> getAllLecturers() {
        return lecturers;
    }

    public void addLecturer(Lecturer lecturer) {
        this.lecturers.add(lecturer);
    }

    /**
     * This is only needed for lecturers since Many-To-Many is used
     */
    @PreRemove
    private void removeDependencies() {
        for (Lecturer l : lecturers) {
            l.removeRequirement(this);
        }
    }
}

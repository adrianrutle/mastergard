import React from "react";
import { TouchableOpacity, TouchableNativeFeedback, Platform, Text, View, StyleSheet } from "react-native";

const ButtonWithBackground = props => {
    const content = (
        <View style={[styles.button, props.style]}>
            <Text style={styles.text}>{props.children}</Text>
        </View>
    );
    if (Platform.OS === 'android') {
        return (
            <TouchableNativeFeedback onPress={props.onPress}>
                {content}
            </TouchableNativeFeedback>
        );
    }
    return <TouchableOpacity onPress={props.onPress}>{content}</TouchableOpacity>
};


const styles = StyleSheet.create({
    button: {
        padding: 10,
        marginTop: 14,
        marginBottom: 14,
        borderRadius: 10,
        justifyContent: 'center',

    },
    text: {
        textAlign: 'center',
        color: 'white',
        fontSize: 25
    }
});

export default ButtonWithBackground;
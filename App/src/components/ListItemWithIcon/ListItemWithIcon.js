import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Platform } from "react-native";
import IconMaterial from "react-native-vector-icons/MaterialIcons"
import IconIonic from "react-native-vector-icons/Ionicons"

const listItemWithIcon = props => (
    <TouchableOpacity onPress={props.onItemPressed}>
        <View style={styles.listItem}>

            <View style={styles.buttonContainer}>
                <IconMaterial name={props.iconId} size={25} color={'#019645'} />
            </View>


            <View style={styles.textContainer}>
                <Text style={styles.textStyle}>{props.textProp}</Text>
                <IconIonic style={styles.arrowButton} name={Platform.OS === 'android' ? "md-arrow-forward" : "ios-arrow-forward"} size={15} />
            </View>


        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    listItem: {
        width: "100%",
        flexDirection: "row",
        alignItems: "center",

    },
    buttonContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        marginRight: 15,
        marginLeft: 15,

    },
    textContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: "row",
    },
    textStyle: {
        fontSize: 16,
        //arginBottom: 15,
        color: 'black'
    },
    arrowButton: {
        marginTop: 3,
        marginRight: 20,
    }
});

export default listItemWithIcon;
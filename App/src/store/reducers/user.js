import {
    SET_STUDENT,
    SET_LECTURER,
    REMOVE_LECTURER,
    REMOVE_STUDENT
} from "../actions/actionTypes";

const initialState = {
    student: null,
    lecturer: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_STUDENT:
            return {
                ...state,
                student: action.student,
            };

        case REMOVE_STUDENT:
            return {
                ...state,
                student: null,
            };

        case SET_LECTURER:
            return {
                ...state,
                lecturer: action.lecturer,
            };

        case REMOVE_LECTURER:
            return {
                ...state,
                lecturer: null,
            };

        default:
            return state;
    }
};

export default reducer;

import React, { Component } from 'react'
import { WebView } from 'react-native'
import { connect } from "react-redux";
import withDeepLinking from '../../components/DeepLinkingHandler';

class Feide extends Component {
    render() {
        return (
            <WebView
                //prod 
                source={{ uri: 'https://auth.dataporten.no/accountchooser?request=%7B%22return%22%3A%22https%3A%5C%2F%5C%2Fauth.dataporten.no%5C%2Foauth%5C%2Fauthorization%3Fclient_id%3D7053f726-7652-47ef-8ece-9991ac315046%26redirect_uri%3Dhttps%253A%252F%252Ftjenestekort.herokuapp.com%252Foauth-redirect.html%26response_type%3Did_token%2520token%26scope%3Dprofile%2520groups%2520person%2520eduPersonPrimaryAffiliation%2520eduPersonAffiliation%2520openid%2520userid-feide%26state%3D65bbca270ce04039a26b2d77887d4765%26nonce%3Dcccc3aee4066433ba72a92611c574834%22%2C%22clientid%22%3A%227053f726-7652-47ef-8ece-9991ac315046%22%7D' }}
            //dev 
            // source={{ uri: 'https://auth.dataporten.no/accountchooser?request=%7B%22return%22%3A%22https%3A%5C%2F%5C%2Fauth.dataporten.no%5C%2Foauth%5C%2Fauthorization%3Fclient_id%3D7053f726-7652-47ef-8ece-9991ac315046%26redirect_uri%3Dhttp%253A%252F%252F10.111.13.68%253A8080%252Foauth-redirect.html%26response_type%3Did_token%2520token%26scope%3Dprofile%2520groups%2520person%2520eduPersonPrimaryAffiliation%2520eduPersonAffiliation%2520openid%2520userid-feide%26state%3Dae60ac053b0344fc877d46bf768fb623%26nonce%3Dabed2adbf6e9458d999a7f149afbf8b6%22%2C%22clientid%22%3A%227053f726-7652-47ef-8ece-9991ac315046%22%7D' }}
            />
        )
    }
}

export default connect(null, null)(withDeepLinking(Feide));
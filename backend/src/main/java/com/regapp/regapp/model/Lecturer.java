package com.regapp.regapp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "LECTURER")
public class Lecturer {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String name;
    @NotBlank
    private String email;

    private String userid;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonBackReference
    private List<Requirement> requirements = new ArrayList<>();

    public Lecturer() {
    }

    public Long getId() {
        return id;
    }

    public Lecturer(@NotBlank String name, @NotBlank String email) {
        this.name = name;
        this.email = email;
    }

    public Lecturer(@NotBlank String name, @NotBlank String email, @NotBlank String userid) {
        this.name = name;
        this.email = email;
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void addRequirement(Requirement requirement) {
        this.requirements.add(requirement);
    }

    public void addRequirements(List<Requirement> requirements) {
        this.requirements.addAll(requirements);
    }

    public void removeRequirement(Requirement requirement) {
        this.requirements.remove(requirement);
    }

    /**
     * Will remove all requirements that is associated with the lecturer
     * @param courseId id of course
     */
    public void removeLecturerFromCourse(Long courseId) {
        List<Requirement> updatedRequirements = new ArrayList<>();

        for (Requirement requirement : this.requirements) {
            if(!requirement.getCourse().getId().equals(courseId)) {
                updatedRequirements.add(requirement);
            }
        }
        
        this.requirements = updatedRequirements;
    }
}

package com.regapp.regapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override

    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/").permitAll()

                //Auth
                .antMatchers(
                        "/role"
                ).hasAnyRole("STUDENT","LECTURER","ADMIN")

                //Course
                .antMatchers(
                        HttpMethod.GET,
                        "/course"
                ).hasAnyRole("STUDENT","LECTURER","ADMIN")

                .antMatchers(
                        HttpMethod.POST,
                        "/course"
                ).hasRole("ADMIN")

                .antMatchers(
                        HttpMethod.GET,
                        "/course/{id}"
                ).hasAnyRole("STUDENT","LECTURER","ADMIN")


                .antMatchers(
                        HttpMethod.GET,
                        "/course/requirement/{id}"
                ).hasRole("ADMIN")


                .antMatchers(
                        HttpMethod.GET,
                        "/course/student/requirement/{courseId}/{studentId}"
                ).hasRole("STUDENT")

                .antMatchers(
                        HttpMethod.GET,
                        "/course/lecturer/requirement/{courseId}/{lecturerId}"
                ).hasRole("LECTURER")

                .antMatchers(
                        HttpMethod.GET,
                        "/course/lecturer/{id}"
                ).hasRole("ADMIN")

                .antMatchers(
                        HttpMethod.PUT,
                        "/course/{id}"
                ).hasRole("ADMIN")

                .antMatchers(
                        HttpMethod.DELETE,
                        "/course/{cid}"
                ).hasRole("ADMIN")

                .antMatchers(
                        HttpMethod.PUT,
                        "/course/lecturer/remove/{lecturerId}/{courseId}"
                ).hasRole("ADMIN")
                .antMatchers(
                        HttpMethod.GET,
                        "/course/users/{courseId}"
                ).hasAnyRole("LECTURER","ADMIN")


                //Student
                .antMatchers(
                        HttpMethod.GET,
                        "/student/result"
                ).hasRole("ADMIN")

                .antMatchers(
                        HttpMethod.POST,
                        "/student/feide/{feideId}"
                ).hasRole("STUDENT")


                //Lecturer
                .antMatchers(
                        HttpMethod.GET,
                        "/lecturer"
                ).hasRole("ADMIN")

                .antMatchers(
                        HttpMethod.POST,
                        "/lecturer/feide/{feideId}"
                ).hasRole("LECTURER")

                .antMatchers(
                        HttpMethod.GET,
                        "/lecturer/requirement/signature/{lecturerId}/{requirementId}"
                ).hasRole("LECTURER")

                .antMatchers(
                        HttpMethod.PUT,
                        "/lecturer/requirement/sign"
                ).hasRole("STUDENT")

                .antMatchers(
                        HttpMethod.POST,
                        "/lecturer/requirement/{lecturerId}"
                ).hasRole("ADMIN")

                //Requirement
                .antMatchers(
                        HttpMethod.GET,
                        "/requirement/data/{id}"
                ).hasRole("ADMIN");

    }

}

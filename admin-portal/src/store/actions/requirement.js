import { SET_REQUIREMENTS, REMOVE_REQUIREMENTS, SELECTED_REQUIREMENT, SELECTED_REQUIREMENT_DATA } from "./actionTypes";
import { uiStartLoading, uiStopLoading, showSuccess } from "./ui";
import { browserHistory } from '../../App'
import { responseHandler, fetchAPI, fetchAPIWithRequestBody } from './actionHelper';


export const getRequirementForCourse = () => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())
        fetchAPI('/course/requirement/' + getState().courses.selectedCourse.id, 'GET', getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(requirements => {
                        dispatch(storeRequirement(requirements))
                        dispatch(uiStopLoading())
                    })
            })
    }
}

export const addCourseRequirementsToLecturer = (requirements) => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())
        fetchAPIWithRequestBody(
            '/lecturer/requirement/' + getState().lecturer.selectedLecturer.id
            , 'POST', JSON.stringify(requirements)
            , getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(() => {

                        dispatch(uiStopLoading())
                        browserHistory.goBack()

                        dispatch(
                            showSuccess(
                                getState().lecturer.selectedLecturer.name +
                                " er nå tilknyttet " +
                                getState().courses.selectedCourse.title)
                        )
                    })
            })
    }
}

export const getRequirementData = () => {
    return (dispatch, getState) => {
        dispatch(uiStartLoading())
        fetchAPI('/requirement/data/' + getState().requirements.selectedRequirement.id, 'GET', getState().auth.token)
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
            }).then(response => {
                dispatch(responseHandler(response))
                    .then(requirementData => {
                        dispatch(storeRequirementData(requirementData))
                        dispatch(uiStopLoading())
                    })
            })
    }
}
export const selectRequirement = (requirement) => {
    return (dispatch) => {
        dispatch({
            type: SELECTED_REQUIREMENT,
            requirement: requirement
        })
    }
}

export const storeRequirementData = (requirementData) => {
    return (dispatch) => {
        dispatch({
            type: SELECTED_REQUIREMENT_DATA,
            requirementData: requirementData
        })
    }
}

export const storeRequirement = (requirements) => {
    return {
        type: SET_REQUIREMENTS,
        requirements: requirements
    }
}

export const removeRequirements = () => {
    return {
        type: REMOVE_REQUIREMENTS
    }
}
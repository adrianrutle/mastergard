import React from "react";
import { TextField } from 'react-native-material-textfield';
import { TextInput, StyleSheet,Text, View} from "react-native";

const defaultInput = props => (
    <View style ={styles.inputContainer }>
        <TextInput  
        {...props}
        style={[
            styles.input, 
            props.style, 
            !props.valid && props.touched ? styles.invalid : null]
        } 
        />
      
            {/* <Text style={styles.errorText}>Error Message</Text> */}
    </View>
    
);

const styles = StyleSheet.create({
    inputContainer: {
         width: "100%",
          borderColor: "red",
          borderColor: "#eee",
          borderRadius: 5,
    },
    input: {
        // height: 35,
        // fontSize: 18,
        // borderWidth: 1,
        // //borderColor: "red",
        // borderColor: "#eee",
        // borderRadius: 5,
        // padding: 5,
        // marginTop: 14,
        // marginBottom: 14
    },
    errorText: {

    },
    invalid: {
        //backgroundColor: '#f9c0c0',
        //borderColor: "red"
    }
});

export default defaultInput;

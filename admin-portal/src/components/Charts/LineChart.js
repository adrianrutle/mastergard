import React from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

const line = (selectedCourse) => {
    return {
        title: {
            text: 'Signeringer per dag'
        },
        yAxis: {
            title: {
                text: 'Antall signeringer'
            }
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                month: '%e. %b',
                year: '%b'
            },
            title: {
                text: 'Dato'
            }
        },
        tooltip: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        series: [{
            name: "Winter 2014-2015",
            data: [
                [Date.UTC(1970, 1, 1), 5],
                [Date.UTC(1970, 1, 2), 3],
                [Date.UTC(1970, 1, 3), 0],
                [Date.UTC(1970, 1, 4), 0],
                [Date.UTC(1970, 1, 5), 5],
                [Date.UTC(1970, 1, 6), 2]
            ]
        }]

    }
}
const LineChart = (props) => {
    return (
        <div>
            <HighchartsReact
                highcharts={Highcharts}
                options={line(props.data)}
            />
        </div>
    )
}
export default LineChart;

import { browserHistory } from '../../App'
import { showError } from './ui';
import { logout } from './auth'
export const responseHandler = response => {
    return (dispatch) => {
        const promise = new Promise((resolve, reject) => {
            if (response.status === 401) {
                browserHistory.replace('/')
                dispatch(logout())
                dispatch(showError("Du må logge inn på nytt"))
                reject()
            }
            if (response.status === 404) {
                dispatch(showError("Noe gikk galt, prøv igjen senere"))
                reject()
            }
            if (response.status === 500) {
                dispatch(showError("Noe gikk galt, prøv igjen senere"))
                reject()
            }
            if (response.status === 200) {
                response.json().then(responseBody => {
                    resolve(responseBody)
                })
            }
        });
        promise.catch(err => {

            console.log('errrrr:' + err);
        });
        return promise;
    }

}

export const fetchAPI = (route, httpMethod, token) => {
    return fetch(route, {
        method: httpMethod,
        headers: {
            "Content-type": "application/json",
            "Authorization": "Bearer " + token
        }
    })
}
export const fetchAPIWithRequestBody = (route, httpMethod, requestBody, token) => {
    return fetch(route, {
        method: httpMethod,
        headers: {
            "Content-type": "application/json",
            "Authorization": "Bearer " + token
        },
        body: requestBody
    })
}

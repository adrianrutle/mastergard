export {
    doAuthorization,
    doLogout
} from './auth'

export {
    getCourses,
    updateCourseInfo,
    selectCourse,
    uploadNewCourse,
    getNumberOfUsersInCourse
} from './course'

export {
    getAvailableLecturers,
    getCourseLecturers,
    selectLecturer,
    removeLecturerFromCourse
} from './lecturer'


export {
    showError,
    hideAlert,
    showInfo
} from './ui'

export {
    getRequirementForCourse,
    addCourseRequirementsToLecturer,
    selectRequirement,
    getRequirementData
} from './requirement'
export {
    getStudentResults
} from './admin'


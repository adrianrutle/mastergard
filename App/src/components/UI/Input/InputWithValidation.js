import React, {Component} from 'react';
import { TextField } from 'react-native-material-textfield';
import {StyleSheet,View} from "react-native";
import validate from "../../../utility/validation";
class LoginInput extends Component {
    //prefix icon ?
    
    state = {
        errorMessage: " ",
        value: "",
        valid: false
    }
    onFocus = () => {
        this.setState( { errorMessage: ' '});
    }
    onBlur = () => {
       this.validateValue(this.state.value)
    }
    setError = (errorMessage) => {
        this.setState(prevState => {
            return {
                ...prevState,
                errorMessage: errorMessage,
                valid: false
            }
        });
    }
    makeValid = () => {
        this.setState(prevState => {
            return {
                ...prevState,
                errorMessage: ' ',
                valid: true
            }
        });
    }
    validateCurrentValue =() =>  {
        this.validateValue(this.state.value);
    }

    validateValue = (value) => {
        this.props.onChildChangeText(value) //
        var validatorOutput = validate(
            value,
            this.props.validationRules
        );
        this.props.isChildValid(validatorOutput.isValid)
        this.setState(prevState => {
            return {
                ...prevState,
                value: value,
                valid: validatorOutput.isValid,
                errorMessage: validatorOutput.isValid ? " " : validatorOutput.errorMessage
                }
        });
    } 

    render() {              
        return (
            <TextField  
                {...this.props}
                textColor = "black"
                labelHeight	= {12}
                labelFontSize = {12}
                titleFontSize = {13}
                tintColor = "black"
                baseColor = "black"
                activeLineWidth = {0.5}
                titleTextStyle = {styles.errorStyle}
                onChangeText = {(val) => this.validateValue(val)}
                onBlur = {() => this.onBlur()}
                onFocus = {() => this.onFocus()}
                value={this.state.value}
                title = {this.state.errorMessage}
                errorColor = "black"
                containerStyle ={styles.containerStyle}
                style = {[
                    styles.input, 
                    this.props.style]} 
                />
            );
    
    }
}

const styles = StyleSheet.create({
    errorStyle : {
        color: "red",
        alignSelf: 'flex-end'
    },
    containerStyle: {
        marginBottom: '6%'
    },
    input: {
        fontSize: 20, 
    },
});

export default LoginInput;

import { createDeepLinkingHandler } from 'react-native-deep-link';
import { handleDeepLinking } from '../store/actions'

export default createDeepLinkingHandler([{
    name: 'regapp:',
    routes: [{
        expression: '/feide/:token',
        callback: handleDeepLinking
    }]
}]);
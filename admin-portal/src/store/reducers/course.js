import {
    SET_COURSES,
    REMOVE_COURSES,
    SELECTED_COURSE,
    SELECTED_COURSE_DATA,
} from "../actions/actionTypes";

const initialState = {
    courses: [],
    selectedCourse: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SELECTED_COURSE:
            return {
                ...state,
                selectedCourse: action.course
            };
        case SELECTED_COURSE_DATA:
            let selectedCourse = state.selectedCourse
            selectedCourse['courseData'] = [action.courseData][0];
            return {
                ...state,
                selectedCourse: selectedCourse
            };
        case SET_COURSES:
            return {
                ...state,
                courses: action.courses
            };

        case REMOVE_COURSES:
            return {
                ...state,
                courses: [],
                selectedCourse: {}
            };
        default:
            return state;
    }
};

export default reducer;

package com.regapp.regapp.controller;

import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.service.Requirement.RequirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class RequirementController {
    @Autowired
    RequirementService requirementService;

 /*   //Get all requirements
    @GetMapping("/requirement")
    public List<Requirement> getAllRequirements() {
        return requirementService.getAllRequirements();
    }

    //Get all Students for requirement
    @GetMapping("/requirement/{id}")
    public List<Student> getAllRelatedStudents(@PathVariable(value = "id") Long requirementID) {
        return requirementService.getAllRelatedStudents(requirementID);
    }*/

    @GetMapping("/requirement/data/{id}")
    public String getRequirementData(@PathVariable(value = "id") Long requirementId) {
        return requirementService.getRequirementData(requirementId);
    }
}


import { showAlert, uiStopLoading } from './ui'

import { appConfig, storeArrayInStorage, clearStorage, getItemFromStorage } from '../util'
import { SET_LECTURER, REMOVE_LECTURER } from "./actionTypes";
import startMainTabs from '../../screens/MainTabs/startMainTabs'

export const fetchLecturer = accessToken => {
    return dispatch => {
        fetch(appConfig.userInfoUrl, {
            method: 'GET',
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + accessToken
            }
        }).catch(err => {
            console.log(err);
        })
            .then(res => res.json())
            .then(lecturer => {

                dispatch(setFeideIdToLecturer(lecturer.user.userid, accessToken))
            });
    }
}

export const setFeideIdToLecturer = (feideId, accessToken) => {
    return (dispatch) => {
        fetch(appConfig.backendUrl + 'lecturer/feide/' + feideId, {
            method: 'POST',
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + accessToken
            }
        }).then(res => {
            res.json().then(lecturerToStore => {
                dispatch(uiStopLoading())
                dispatch(storeLecturer(lecturerToStore));
                startMainTabs("ROLE_LECTURER")
            }
            )
        })

    }
}

export const getLecturer = () => {
    return (dispatch, getState) => {
        const promise = new Promise((resolve, reject) => {
            const lecturer = getState().user.lecturer;
            if (!lecturer) {
                getItemFromStorage('lecturer:user')
                    .catch(err => reject())
                    .then(jsonLecturer => {
                        var lecturer = JSON.parse(jsonLecturer)
                        if (!lecturer) {
                            reject();
                            return;
                        }
                        dispatch(setLecturer(lecturer));
                        resolve(lecturer);
                    })
            } else {
                resolve(lecturer);
            }
        });
        promise.catch(err => {
            dispatch(clearStorage())
        });
        return promise;
    };
}

export const storeLecturer = user => {
    var lecturerInfo = {
        name: user.name,
        email: user.email,
        userid: user.userid,
        id: user.id
    }
    return dispatch => {
        dispatch(setLecturer(lecturerInfo));
        storeArrayInStorage('lecturer:user', lecturerInfo)
    }
}
export const setLecturer = lecturer => {
    return {
        type: SET_LECTURER,
        lecturer: lecturer
    }
};

export const removeLecturer = () => {
    return {
        type: REMOVE_LECTURER
    }
}

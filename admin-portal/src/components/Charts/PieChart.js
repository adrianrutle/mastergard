import React from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

const line = (requirementData) => {
    var signed = 0;
    var unSigned = 9;

    requirementData.forEach(data => {
        if (data.isRequirementSigned) {
            signed++
        } else {
            unSigned++
        }
    });
    return {
        title: {
            text: 'Signeringer',
            align: 'center',
            verticalAlign: 'middle',
            y: 50
        },
        credits: {
            enabled: false
        },
        colors: ["#f7a35c", "#90ed7d"],
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    },
                    format: '{point.name}: <b>{point.percentage:.1f}%</b>'
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%'],
                size: '110%'
            }
        },
        series: [{
            type: 'pie',
            name: 'Studenter',
            innerSize: '50%',
            data: [{
                name: 'Ikke signert',
                y: unSigned
            },
            {
                name: 'Signert',
                y: signed
            },

            ]
        }]
    }
}
const PieChart = (props) => {
    return (
        <div>
            <HighchartsReact
                highcharts={Highcharts}
                options={line(props.data)}
            />
        </div>
    )
}
export default PieChart;
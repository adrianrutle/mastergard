package com.regapp.regapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "COURSE")
public class Course {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String title;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "course_id")
    private List<Requirement> requirements = new ArrayList<>();

    public Course(String title) {
        this.title = title;
    }

    public Course() {
    }


    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    //TODO legg til course
    @JsonIgnore
    public List<Requirement> getRequirements() {
        return requirements;
    }

    @JsonIgnore
    public List<Student> getCourseStudents() {
        List<Student> students = new ArrayList<>();
        for (Requirement requirement :requirements) {
            for (Student student : requirement.getAllStudents()) {
                if(!students.contains(student)){
                    students.add(student);
                }
            }
            
        }
        return students;
    }

    public void addRequirement(Requirement requirement) {
        requirements.add(requirement);
        requirement.setCourse(this);
    }

    public void addRequirements(List<Requirement> requirements) {
        for (Requirement req : requirements) {
            this.requirements.add(req);
            req.setCourse(this);
        }
    }

    public void removeRequirement(Requirement requirement) {
        requirements.remove(requirement);
        //requirement.setCourse(null);
    }

}

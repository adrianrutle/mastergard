export const getStudentResults = () => {
    return (dispatch, getState) => {
        fetch('/student/result', {
            method: 'GET',
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + getState().auth.token,
            },
        })
            .catch(err => {
                console.log(err);
            }).then(response => {
                const filename = response.headers.get('Content-Disposition').split('filename=')[1];
                response.blob().then(blob => {
                    let url = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = url;
                    a.download = filename;
                    a.click();
                })
            })
    }
}
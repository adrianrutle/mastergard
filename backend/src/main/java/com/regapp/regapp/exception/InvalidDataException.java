package com.regapp.regapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidDataException extends RuntimeException {
    private String fieldName;

    public InvalidDataException( String fieldName) {
        super(String.format("The %s is not valid.", fieldName));
        this.fieldName = fieldName;
    }
    public String getFieldName() {
        return fieldName;
    }
}

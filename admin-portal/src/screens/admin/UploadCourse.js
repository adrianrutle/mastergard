import React, { Component } from 'react'
import Dropzone from 'react-dropzone'
import readXlsxFile from 'read-excel-file'
import LoadingSpinner from '../../components/LoadingSpinner';
import { connect } from 'react-redux';

import { Button, Container, Form, FormGroup, Label, Input, Table, Collapse } from 'reactstrap';
import { uploadNewCourse } from '../../store/actions';

class UploadCourse extends Component {
    state = {
        courseTitle: "",
        students: [],
        requirements: [],
        lecturers: [],
        studentRequirementGroups: [],
        collapse: {
            state: false,
            type: ''
        },
        files: []

    }
    handleOnDrop = (files) => {

        readXlsxFile(files[0]).then((rows, e) => {
            var students = [];
            var requirements = [];
            var lecturers = [];
            var courseTitle = '';
            var studentRequirements = [];

            rows.forEach((col, i) => {

                var studLastName = col[0];
                var studFirstName = col[1];
                var studEmail = col[2];
                var studentRequirementGroup = col[3];

                courseTitle = col[4];
                var courseReqSubject = col[5];
                var courseReqTitle = col[6];
                var requirementDate = col[8];
                var lecturerName = col[9];
                var lecturerEmail = col[10];
                if (i > 0) {
                    //Students
                    if (studLastName && studFirstName && studEmail && studentRequirementGroup !== null) {
                        if (!students.some(stud => stud.email === studEmail)) {
                            students.push({
                                name: studFirstName + " " + studLastName,
                                email: studEmail,
                                group: studentRequirementGroup
                            })
                        }

                    }

                    //Requirements
                    if (courseReqSubject && courseReqTitle !== null) {
                        if (!requirements.some(requ => requ.title === courseReqTitle)) {
                            requirements.push({
                                subject: courseReqSubject,
                                title: courseReqTitle
                            })

                        }
                    }

                    //StudentRequirement
                    if (studentRequirementGroup && requirementDate && courseReqSubject && courseReqTitle && studLastName && studFirstName && studEmail !== null) {
                        const dateToFormat = new Date(requirementDate);
                        if (!studentRequirements.some(sr => sr.student.email === studEmail)) {
                            studentRequirements.push({
                                student: {
                                    name: studFirstName + " " + studLastName,
                                    email: studEmail,
                                },
                                requirements: [{
                                    title: courseReqTitle,
                                    subject: courseReqSubject,
                                    group: studentRequirementGroup,
                                    date: dateToFormat.toLocaleDateString()
                                }]
                            })
                        } else {
                            if (!studentRequirements.some(sr => sr.requirement === courseReqTitle)) {
                                studentRequirements.forEach(sr => {
                                    if (sr.student.email === studEmail) {
                                        sr.requirements.push({
                                            title: courseReqTitle,
                                            subject: courseReqSubject,
                                            group: studentRequirementGroup,
                                            date: dateToFormat.toLocaleDateString(),
                                        })
                                    }
                                });
                            }
                        }
                    }

                    //Lecturers
                    if (lecturerName && lecturerEmail !== null) {
                        if (!lecturers.some(lec => lec.email === lecturerEmail)) {
                            lecturers.push({
                                name: lecturerName,
                                email: lecturerEmail
                            })
                        }

                    }
                }

            });
            this.setState(state => {
                return {
                    ...state,
                    courseTitle: courseTitle,
                    students: students,
                    requirements: requirements,
                    lecturers: lecturers,
                    studentRequirements: studentRequirements,
                    files: files
                }

            })

        })

    }
    handleCollapseToggle = (buttonType) => {
        this.setState(state => {
            var collapse = {}
            if (this.state.collapse.state) {
                collapse = {
                    state: !this.state.collapse.state,

                }
            } else {
                collapse = {
                    state: !this.state.collapse.state,
                    type: buttonType
                }
            }
            return {
                ...state,
                collapse: collapse
            }

        })
    }


    populateStudentTable() {
        if (this.state.students.length > 0) {
            return this.state.students.map((student, i) => {
                return <tr key={i + 1}>
                    <td >{i + 1}</td>
                    <td >{student.name}</td>
                    <td >{student.email}</td>
                    <td >{student.group}</td>
                </tr>
            });
        }
    }

    populateRequirementTable() {
        if (this.state.requirements.length > 0) {
            return this.state.requirements.map((requirement, i) => {
                return <tr key={i + 1}>
                    <td >{i + 1}</td>
                    <td >{requirement.subject}</td>
                    <td >{requirement.title}</td>

                </tr>
            });
        }
    }

    populateLecturerTable() {
        if (this.state.requirements.length > 0) {
            return this.state.lecturers.map((lecturer, i) => {
                return <tr key={i + 1}>
                    <td >{i + 1}</td>
                    <td >{lecturer.name}</td>
                    <td >{lecturer.email}</td>

                </tr>
            });
        }
    }

    onFormSubmit = (e) => {
        e.preventDefault()
        this.props.uploadNewCourse(this.state)
    }

    render() {
        if (this.props.isLoading) {
            return (<LoadingSpinner />)
        }


        const files = this.state.files.map(file => (
            <li key={file.name}>
                {file.name}
            </li>
        ))

        const studentsTable = (
            (this.state.collapse.type === "students")
                ? (<Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Navn</th>
                            <th>Epost</th>
                            <th>Gruppe</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.populateStudentTable()}
                    </tbody>
                </Table>) : undefined
        )

        const requirementsTable = (
            (this.state.collapse.type === "requirements")
                ? (<Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fagområde</th>
                            <th>Prosedyre/aktivitet</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.populateRequirementTable()}
                    </tbody>
                </Table>) : undefined
        )

        const lecturersTable = (
            (this.state.collapse.type === "lecturers")
                ? (<Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Epost</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.populateLecturerTable()}
                    </tbody>
                </Table>) : undefined
        )

        const getTableCollapseButtons = (
            (this.state.students.length > 1 &&
                this.state.requirements.length > 1 &&
                this.state.lecturers.length > 1)
                ? (<div>
                    <Button color="info" style={{ marginRight: 10 }} onClick={() => this.handleCollapseToggle('students')}>Studenter</Button>
                    <Button color="info" style={{ marginRight: 10 }} onClick={() => this.handleCollapseToggle('requirements')}>Aktiveter</Button>
                    <Button color="info" style={{ marginRight: 10 }} onClick={() => this.handleCollapseToggle('lecturers')}>Undervisere</Button>
                    <Collapse isOpen={this.state.collapse.state}>
                        {studentsTable}
                        {requirementsTable}
                        {lecturersTable}
                    </Collapse>
                </div>) : <div>
                    <Button disabled color="info" style={{ marginRight: 10 }}>Studenter</Button>
                    <Button disabled color="info" style={{ marginRight: 10 }}>Aktiveter</Button>
                    <Button disabled color="info" style={{ marginRight: 10 }} >Undervisere</Button>
                </div>)

        return (
            <Container>
                <h2 className={"header"}>Last opp ett nytt emne</h2>
                <div className={"Form"}>
                    {<Form onSubmit={this.onFormSubmit} >
                        <FormGroup >
                            <Label for="courseTitle">Tittel på emne:</Label>
                            <Input name="title"
                                id="courseTitle"
                                value={this.state.courseTitle}
                                onChange={e => this.setState({ courseTitle: e.target.value })}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="courseInfo">Emnebeskrivelse:</Label>
                            <Input type="textarea" name="text" id="courseInfo" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="courseInfo">Last opp emneinformasjon:</Label>
                            <Dropzone
                                name={"Emneinformasjon"}
                                onDrop={this.handleOnDrop}
                                multiple={false}
                                accept={".xlsx"}
                                maxSize={1000000000}>

                            </Dropzone>
                            <h4>Fil:</h4>
                            <ul>{files}</ul>

                        </FormGroup>

                        <FormGroup>
                            <Label for="courseInfo">Sjekk at informasjonen stemmer:</Label>
                            {getTableCollapseButtons}
                        </FormGroup>
                        <Button color="success" type="submit">Lagre</Button>
                    </Form>}
                </div>

            </Container>)
    }
}

const mapStateToProps = (state) => {
    return {
        /* courses: state.courses.courses,
        isLoading: state.ui.isLoading */
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        uploadNewCourse: (courseInfo) => dispatch(uploadNewCourse(courseInfo))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(UploadCourse);

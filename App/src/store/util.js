import { AsyncStorage } from "react-native";
import { Alert } from "react-native";

const localIp = '10.111.51.82'
const dev = 'http://' + localIp + ':8080/'
const prod = 'https://tjenestekort.herokuapp.com/'
export const appConfig = {
    backendUrl: prod,
    userInfoUrl: 'https://auth.dataporten.no/userinfo/',
    oAuthConfig: {
        clientId: '7053f726-7652-47ef-8ece-9991ac315046',
        clientSecret: '2571e762-f8cd-4a13-a4c5-2f4244e24522',
        //http://localhost:8080/oauth-redirect.html
        redirectUrl: 'http://10.111.49.91:8080/oauth-redirect.html',
        //redirectUrl: 'com.herokuapp.tjenestekort://',
        //'profile','groups','eduPersonAffiliation','email','mail'
        scopes: ['profile',
            'groups',
            'person',
            'eduPersonPrimaryAffiliation',
            'eduPersonAffiliation',
            'openid',
            'userid-feide'], //'openid','groups'eduPersonAffiliation
        serviceConfiguration: {
            authorizationEndpoint: 'https://auth.dataporten.no/oauth/authorization',
            tokenEndpoint: 'https://auth.dataporten.no/oauth/token'
        }
    }
}

export const storeArrayInStorage = (key, array) => {
    return AsyncStorage.setItem(key, JSON.stringify(array));
}

export const storeItemInStorage = (key, value) => {
    return AsyncStorage.setItem(key, value);
}

export const getItemFromStorage = (key) => {
    return AsyncStorage.getItem(key);
}

export const setAlert = (title, message, func) => {

    Alert.alert(
        title,
        message,
        [
            { text: 'OK', onPress: () => dispatch(func) },
        ]
    )
}

/**
 * Clear local storage
 */
export const clearStorage = () => {
    return dispatch => {
        AsyncStorage.removeItem('test:auth:token')
        AsyncStorage.removeItem('auth:affiliation')
        AsyncStorage.removeItem('student:user')
    }
};

export {
    autoSignIn,
    logout,
    doAuth,
    handleDeepLinking
} from './auth'

export {
    fetchStudent,
    checkIfStudentExist,
    getStudent,
    removeStudent
} from './student'

export {
    fetchLecturer,
    checkIfLecturerExist,
    getLecturer,
    removeLecturer
} from './lecturer'

export {
    uiStartLoading,
    uiStopLoading,
    showAlert,
    hideAlert
} from './ui'

export {
    appConfig,
    clearStorage,
    storeItemInStorage,
    getItemFromStorage,
    storeArrayInStorage
} from '../util'

export {
    getToken,
    storeToken,
    removeToken
} from './token'

export {
    getAllCourses,
    removeCourses
} from './course'

export {
    getCourseRequirements,
    getCourseLecturerRequirements,
    signRequirement,
    getQRCodeString
} from './requirement'
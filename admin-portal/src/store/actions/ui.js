import {
    UI_START_LOADING, UI_STOP_LOADING,
    SHOW_ERROR, SHOW_INFO, HIDE_ALERT, SHOW_SUCCESS
} from "./actionTypes";

export const uiStartLoading = () => {
    return {
        type: UI_START_LOADING
    };
};


export const uiStopLoading = () => {

    return {
        type: UI_STOP_LOADING
    };
};


export const showError = message => {
    return {
        type: SHOW_ERROR,
        alertMessage: message
    }
};

export const showInfo = message => {
    return {
        type: SHOW_INFO,
        alertMessage: message
    }
};

export const hideAlert = () => {
    return {
        type: HIDE_ALERT
    }
};

export const showSuccess = message => {
    return {
        type: SHOW_SUCCESS,
        alertMessage: message
    }
};



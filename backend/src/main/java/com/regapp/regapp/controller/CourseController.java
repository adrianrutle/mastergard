package com.regapp.regapp.controller;

import com.regapp.regapp.model.Course;
import com.regapp.regapp.model.Lecturer;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.service.Course.CourseService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CourseController {

    @Autowired
    CourseService courseService;

    /**
     * Used
     */
    @GetMapping("/course")
    public List<Course> getCourses() {
        return courseService.getCourses();
    }

    /**
     * Used
     */
    @PostMapping("/course")
    public Course createCourse(HttpEntity<String> httpEntity) {
        JSONObject requestBody = new JSONObject(httpEntity.getBody());
        String courseTitle = requestBody.get("title").toString();
        String requirements = requestBody.get("requirements").toString();
        String lecturers = requestBody.get("lecturers").toString();
        String studentRequirements = requestBody.get("studentRequirements").toString();

        JSONArray requirementsJsonArr = new JSONArray(requirements);
        JSONArray lecturersJsonArr = new JSONArray(lecturers);
        JSONArray studentRequirementsArr = new JSONArray(studentRequirements);

        //Requirements
        List<Requirement> requirementsToAdd = new ArrayList<>();
        for (int i = 0; i < requirementsJsonArr.length(); i++) {
            JSONObject requirement = requirementsJsonArr.getJSONObject(i);
            requirementsToAdd.add(
                    new Requirement(
                            requirement.getString("subject"),
                            requirement.getString("title"),
                            "KNY6rDBDSS"));
        }

        //Requirements
        List<Lecturer> lecturersToAdd = new ArrayList<>();
        for (int i = 0; i < lecturersJsonArr.length(); i++) {
            JSONObject lecturer = lecturersJsonArr.getJSONObject(i);
            lecturersToAdd.add(
                    new Lecturer(
                            lecturer.getString("name"),
                            lecturer.getString("email")));
        }

        //studentRequirements
        List<StudReq> studReqsToAdd = new ArrayList<>();
        for (int i = 0; i < studentRequirementsArr.length(); i++) {
            JSONObject stuReqInfo = studentRequirementsArr.getJSONObject(i);

            JSONArray stuReqRequirements = stuReqInfo.getJSONArray("requirements");
            JSONObject studentJSON = stuReqInfo.getJSONObject("student");

            for (int j = 0; j < stuReqRequirements.length(); j++) {
                StudReq studReq = new StudReq();
                Student student = new Student(
                        studentJSON.getString("name"),
                        studentJSON.getString("email"));
                studReq.setStudent(student);
                student.addStudRequirement(studReq);
                JSONObject requirement = stuReqRequirements.getJSONObject(j);

                studReq.setRequirement(new Requirement(
                        requirement.getString("subject"),
                        requirement.getString("title"),
                        "KNY6rDBDSS"));

                studReq.setDate(requirement.getString("date"));
                studReq.setStudentGroup(requirement.getString("group"));
                studReq.signRequirement(false);
                studReqsToAdd.add(studReq);
            }
        }
        return courseService.addCourse(courseTitle, requirementsToAdd,lecturersToAdd, studReqsToAdd);
    }

    @GetMapping("/course/{id}")
    public Course getCourseById(@PathVariable(value = "id") Long courseId) {
        return courseService.getCourseById(courseId);
    }

    /**
     * Used
     */
    @GetMapping("/course/requirement/{id}")
    public List<Requirement> getCourseRequirements(@PathVariable(value = "id") Long courseId) {
        return courseService.getCourseRequirements(courseId);
    }

    /**
     * Used
     */
    @GetMapping("/course/student/requirement/{courseId}/{studentId}")
    public List<StudReq> getCourseRequirementsForStudent(
            @PathVariable(value = "courseId") Long courseId,
            @PathVariable(value = "studentId") Long studentId) {
        return courseService.getCourseRequirementsForStudent(courseId, studentId);
    }

    /**
     * Used
     */
    @GetMapping("/course/lecturer/requirement/{courseId}/{lecturerId}")
    public List<Requirement> getCourseRequirementsForLecturer(
            @PathVariable(value = "courseId") Long courseId,
            @PathVariable(value = "lecturerId") Long lecturerId) {
        return courseService.getCourseRequirementsForLecturer(courseId, lecturerId);
    }

    @GetMapping("/course/lecturer/{id}")
    public List<Lecturer> getCourseLecturers(@PathVariable(value = "id") Long courseId) {
        return courseService.getCourseLecturers(courseId);
    }

    /**
     * Used
     */
    @PutMapping("/course/{id}")
    public Course updateCourse(@PathVariable(value = "id") Long courseId,
                               @Valid @RequestBody Course courseDetails) {
        return courseService.updateCourse(courseId, courseDetails);
    }

    @DeleteMapping("/course/{cid}")
    public void deleteCourse(
            @PathVariable(value = "cid") Long courseId) {
        courseService.deleteCourse(courseId);
    }

    @PutMapping("/course/lecturer/remove/{lecturerId}/{courseId}")
    public Lecturer removeLecturerFromCourse(
            @PathVariable(value = "courseId") Long courseId,
            @PathVariable(value = "lecturerId") Long lecturerId) {
        return courseService.removeLecturerFromCourse(courseId, lecturerId);
    }
    /**
     * Used
     */
    @GetMapping("/course/users/{courseId}")
    public String getNumberOfUsersInCourse(@PathVariable(value = "courseId") Long courseId) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("students", courseService.getCourseStudents(courseId).size());
        jsonObject.put("lecturers", courseService.getCourseLecturers(courseId).size());
        return jsonObject.toString();
    }

}

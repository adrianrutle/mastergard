import { Navigation } from 'react-native-navigation';
import IconIonic from 'react-native-vector-icons/Ionicons';
import IoncMaterial from 'react-native-vector-icons/MaterialIcons'
import { Platform } from "react-native";



const startTabs = (affiliation) => {
    Promise.all([
        IconIonic.getImageSource(Platform.OS === 'android' ? "md-home" : "ios-home", 30),
        IoncMaterial.getImageSource("more-horiz", 30)
    ]).then(sources => {
        if (affiliation === "ROLE_LECTURER") {
            //EMPLOYEE
            Navigation.startTabBasedApp({
                tabs: [
                    {   //home
                        screen: "regapp.HomeScreenLecturer",
                        label: "Hjem",
                        title: "Hjem",
                        icon: sources[0],
                    },
                    {
                        //Logout
                        screen: "regapp.LogoutScreen",
                        label: "Logg ut",
                        title: "Logg ut",
                        icon: sources[1],
                    }
                ],
                tabsStyle: {
                    tabBarSelectedButtonColor: "#80deea",
                    tabBarBackgroundColor: "white"
                },
                appStyle: {
                    tabBarSelectedButtonColor: "#80deea",
                    tabBarBackgroundColor: "white",
                    hideBackButtonTitle: true,
                    navBarButtonColor: 'white',
                    navBarBackgroundColor: '#80deea',
                    statusBarTextColorSchemeSingleScreen: Platform.OS === 'ios'
                        ? 'light' : undefined,
                    statusBarColor: Platform.OS === 'android'
                        ? '#4bacb8' : undefined,
                    navBarTextColor: 'white'
                }
            });
        } else if (affiliation === "ROLE_STUDENT") {
            //STUDENT
            Navigation.startTabBasedApp({
                tabs: [
                    {   //home
                        screen: "regapp.HomeScreenStudent",
                        label: "Hjem",
                        title: "Hjem",
                        icon: sources[0],
                    },
                    {
                        //logout
                        screen: "regapp.LogoutScreen",
                        label: "Logg ut",
                        title: "Logg ut",
                        icon: sources[1],
                    }
                ],
                tabsStyle: {
                    tabBarSelectedButtonColor: "#80deea",
                    tabBarBackgroundColor: "white"
                },
                appStyle: {
                    tabBarSelectedButtonColor: "#80deea",
                    tabBarBackgroundColor: "white",
                    hideBackButtonTitle: true,
                    navBarButtonColor: 'white',
                    navBarBackgroundColor: '#80deea',
                    statusBarTextColorSchemeSingleScreen: Platform.OS === 'ios'
                        ? 'light' : undefined,
                    statusBarColor: Platform.OS === 'android'
                        ? '#4bacb8' : undefined,
                    navBarTextColor: 'white'
                }
            });
        } else {
            return null;
        }
    });
};
export default startTabs;

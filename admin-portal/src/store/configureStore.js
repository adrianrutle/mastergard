import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import uiReducer from './reducers/ui';
import courseReducer from './reducers/course';
import authReducer from './reducers/auth';
import lecturerReducer from './reducers/lecturer';
import requirementReducer from './reducers/requirement';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const rootReducer = combineReducers({
    courses: courseReducer,
    ui: uiReducer,
    auth: authReducer,
    lecturer: lecturerReducer,
    requirements: requirementReducer
});

const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['ui']
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistedReducer = persistReducer(persistConfig, rootReducer)

const configureStore = () => {
    return createStore(persistedReducer,
        composeEnhancer(applyMiddleware(thunk)));
};

export default configureStore;
package com.regapp.regapp.service.Lecturer;

import com.regapp.regapp.model.Lecturer;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.jointable.StudReq;

import java.util.List;

public interface LecturerService {

    List<Lecturer> getLecturers();

    Lecturer setFeideIdToLecturer(String feideId);

    Lecturer getLecturerById(Long lecturerId);

    List<Requirement> getAllRequirementsForLecturer(Long lecturerId);

    void deleteLecturer(Long lecturerId);

    StudReq signRequirement(Long studentId, String qrString, Long requirementId);

    Lecturer updateLecturer(Long lecturerId, Lecturer lecturerDetails);

    Lecturer getLecturerByUserid(String userid);

    List<Lecturer> getResponsibleLecturers(Long requirementId);

    String getQRString(Long lecturerId, Long requirementId);

    Lecturer addCourseRequirementsToLecturer(Long lecturerId, List<Long> requirementIds);

    Boolean checkIfUserIsLecturer(String email);
}

package com.regapp.regapp.service.Lecturer;

import com.regapp.regapp.exception.BadRequestException;
import com.regapp.regapp.exception.InvalidDataException;
import com.regapp.regapp.exception.ResourceNotFoundException;
import com.regapp.regapp.model.Lecturer;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.repository.LecturerRepository;
import com.regapp.regapp.repository.RequirementRepository;
import com.regapp.regapp.repository.StudReqRepository;
import com.regapp.regapp.repository.StudentRepository;
import com.regapp.regapp.utils.EncryptDecrypt;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("LecturerService")
public class LecturerServiceImpl implements LecturerService {

    @Autowired
    LecturerRepository lecturerRepository;

    @Autowired
    StudReqRepository studReqRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    RequirementRepository requirementRepository;

    @Override
    public List<Lecturer> getLecturers() {
        return lecturerRepository.findAll();
    }

    @Override
    public Lecturer setFeideIdToLecturer(String feideId){
        Lecturer lecturer = getLoggedInLecturer();
        if(lecturer.getUserid() == null){
            lecturer.setUserid(feideId);
            return lecturerRepository.save(lecturer);
        } else {
            return lecturer;
        }

    }

    @Override
    public Boolean checkIfUserIsLecturer(String email){
        Lecturer lecturerByEmail = lecturerRepository.findLecturerByEmail(email);
        if(lecturerByEmail != null){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Lecturer getLecturerById(Long lecturerId) {
        return lecturerRepository.findById(lecturerId)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));
    }

    @Override
    public List<Requirement> getAllRequirementsForLecturer(Long lecturerId) {
        Lecturer lecturer = lecturerRepository.findById(lecturerId)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));
        return lecturer.getRequirements();
    }

    @Override
    public Lecturer getLecturerByUserid(String userid) {
        if (lecturerRepository.findLecturerByUserid(userid) != null) {
            return lecturerRepository.findLecturerByUserid(userid);
        } else {
            throw new ResourceNotFoundException("Lecturer", "id", userid);
        }
    }

    @Override
    public void deleteLecturer(Long lecturerId) {
        Lecturer lecturer = lecturerRepository.findById(lecturerId)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));
        lecturerRepository.delete(lecturer);
    }

    @Override
    public StudReq signRequirement(Long studentId, String qrString, Long requirementId) {
        String decryptedString = EncryptDecrypt.decrypt(qrString);
        if (decryptedString != null) {
            JSONObject jsonObj = new JSONObject(decryptedString);
            Long lecturerId = Long.valueOf(jsonObj.get("lecturerId").toString());
            Long requirementIdFromQr = Long.valueOf(jsonObj.get("requirementId").toString());
            String signature = jsonObj.get("signature").toString();

            if (!requirementIdFromQr.equals(requirementId)) {
                throw new ResourceNotFoundException("Requirement", "requirementId", requirementId);
            }

            Lecturer lecturer = lecturerRepository.findById(lecturerId)
                    .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));

            Student student = studentRepository.findById(studentId)
                    .orElseThrow(() -> new ResourceNotFoundException("Student", "id", studentId));

            List<StudReq> studReqs = student.getStudReqs();
            for (StudReq sr : studReqs) {
                Requirement r = sr.getRequirement();
                if (r.getId().equals(requirementId)) {
                    if (r.getSignature().equals(signature)) {
                        if (r.getAllLecturers().contains(lecturer)) {
                            sr.setSignedTimestamp();
                            sr.signRequirement(true);
                            StudReq updatedStudReq = studReqRepository.save(sr);
                            return updatedStudReq;
                        } else {
                            throw new InvalidDataException("lecturerId");
                        }

                    } else {
                        throw new InvalidDataException("signature");
                    }
                }
            }
            throw new ResourceNotFoundException("Requirement", "id", requirementId);

        } else {
            throw new BadRequestException("qrString");
        }
    }

    @Override
    public List<Lecturer> getResponsibleLecturers(Long requirementId) {
        Requirement requirement = requirementRepository.findById(requirementId)
                .orElseThrow(() -> new ResourceNotFoundException("Requirement", "requirementID", requirementId));

        return requirement.getAllLecturers();
    }

    @Override
    public Lecturer updateLecturer(Long lecturerId, Lecturer lecturerDetails) {
        Lecturer lecturer = lecturerRepository.findById(lecturerId)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));
        lecturer.setName(lecturerDetails.getName());
        lecturer.setEmail(lecturerDetails.getEmail());
        Lecturer updatedLecturer = lecturerRepository.save(lecturer);
        return updatedLecturer;
    }

    @Override
    public String getQRString(Long lecturerId, Long requirementId) {
        Lecturer lecturer = lecturerRepository.findById(lecturerId)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));
        Requirement requirement = requirementRepository.findById(requirementId)
                .orElseThrow(() -> new ResourceNotFoundException("Requirement", "requirementID", requirementId));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("lecturerId", lecturerId);
        jsonObject.put("requirementId", requirementId);
        jsonObject.put("signature", requirement.getSignature());
        return EncryptDecrypt.encrypt(jsonObject.toString());
    }

    @Override
    public Lecturer addCourseRequirementsToLecturer(Long lecturerId, List<Long> requirementIds) {
        Lecturer lecturer = lecturerRepository.findById(lecturerId)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));
        if(!requirementIds.isEmpty()){

            //Uses the first requirement to get the necessary course information
            Requirement firstRequirement = requirementRepository.findById(requirementIds.get(0))
                    .orElseThrow(() -> new ResourceNotFoundException("Requirement", "requirementID",requirementIds.get(0)));

            lecturer.removeLecturerFromCourse(firstRequirement.getCourse().getId());

            for (Long requirementId : requirementIds) {
                Requirement requirement = requirementRepository.findById(requirementId)
                        .orElseThrow(() -> new ResourceNotFoundException("Requirement", "requirementID", requirementId));
                lecturer.addRequirement(requirement);

            }
        }

        Lecturer updatedLecturer = lecturerRepository.save(lecturer);
        return updatedLecturer;
    }

    private Lecturer getLoggedInLecturer(){
        OAuth2Authentication principal =  (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        Object object=principal.getUserAuthentication().getDetails();
        Map<String, Object> studentDetails = (Map<String, Object>) object;
        String email = studentDetails.get("eduPersonPrincipalName").toString();
        return lecturerRepository.findLecturerByEmail(email);
    }
}


import { UI_START_LOADING, UI_STOP_LOADING, SHOW_ALERT, HIDE_ALERT, SHOW_ALERT_FUNC } from "./actionTypes";

export const uiStartLoading = () => {
    return {
        type: UI_START_LOADING
    };
};


export const uiStopLoading = () => {
    return {
        type: UI_STOP_LOADING
    };
};

export const showAlert = (alertTitle, alertMessage) => {
    return {
        type: SHOW_ALERT,
        alertTitle: alertTitle,
        alertMessage: alertMessage
    }
}

export const showAlertWithFunc = (alertTitle, alertMessage, functionName) => {
    return {
        type: SHOW_ALERT_FUNC,
        alertTitle: alertTitle,
        alertMessage: alertMessage,
        functionName: functionName
    }
}

export const hideAlert = () => {
    return {
        type: HIDE_ALERT
    }
}
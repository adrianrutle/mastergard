import { UI_START_LOADING, UI_STOP_LOADING, SHOW_ALERT, HIDE_ALERT, SHOW_ALERT_FUNC } from "../actions/actionTypes";

const initialState = {
    isLoading: false,
    alert: {
        visible: false,
        alertTitle: "",
        alertMessage: "",
        buttonFunction: undefined
    }
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case UI_START_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case UI_STOP_LOADING:
            return {
                ...state,
                isLoading: false
            };
        case SHOW_ALERT:
            return {
                ...state,
                alert: {
                    visible: true,
                    title: action.alertTitle,
                    message: action.alertMessage
                }
            };

        case SHOW_ALERT_FUNC:
            return {
                ...state,
                alert: {
                    visible: true,
                    title: action.alertTitle,
                    message: action.alertMessage,
                    buttonFunction: action.functionName
                }
            };
        case HIDE_ALERT:
            return {
                ...state,
                alert: {
                    visible: false,
                    title: "",
                    message: "",
                    buttonFunction: undefined
                }
            };
        default:
            return state;
    }
};

export default reducer;
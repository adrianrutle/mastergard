//TODO only usage is get ticket

import React from "react";
import {TouchableOpacity, TouchableNativeFeedback, Platform, Image, View, StyleSheet} from "react-native";
const ImageButton = props => {
    const content = (
        <Image 
        {...props}
        style={styles.image}/>  
    );
    if (Platform.OS === 'android') {
        return (
            <TouchableNativeFeedback onPress={props.onPress}>
                {content}
            </TouchableNativeFeedback>
        );
    }
    return <TouchableOpacity onPress={props.onPress}>{content}</TouchableOpacity>
};


const styles = StyleSheet.create({
    button: {
        backgroundColor: "white",
        padding: 10,
        marginTop: 14,
        marginBottom: 14,
        borderRadius: 5,
        alignSelf: "center"
    },
    image: {
        width: 120,
        height: 120,
        marginBottom: 10,
        borderWidth: 1.5,
        borderColor: '#aaa',
    }
});

export default ImageButton;
import React, { Component } from 'react';
import './App.css';
import Auth from './screens/Auth';
import { Router, Route, Switch } from 'react-router-dom';
import CourseSelect from './screens/CourseSelect';
import UpdateCourse from './screens/UpdateCourse';
import CourseRequirements from './screens/CourseRequirements';
import Requirement from './screens/Requirement';
import ChooseModule from './screens/ChooseModule';
import UploadCourse from './screens/admin/UploadCourse';
import Lecturers from './screens/Lecturers';
import AdminModule from './screens/admin/AdminModule';
import SelectRequirements from './screens/SelectRequirements';

import CourseDashboard from './screens/CourseDashboard';
import createBrowserHistory from 'history/createBrowserHistory';
import AppNavbar from './components/AppNavbar';
import { connect } from 'react-redux'
import { hideAlert } from './store/actions'



export const browserHistory = createBrowserHistory();

class App extends Component {

  render() {
    browserHistory.listen(() => {
      this.props.hideAlert()
    });

    return (
      <Router history={browserHistory}>

        <Switch>
          <Route path='/' exact={true} component={HOC(Auth)} />
          <Route path='/course/update' exact={true} component={HOC(UpdateCourse)} />
          <Route path='/courses' exact={true} component={HOC(CourseSelect)} />
          <Route path='/course/requirements' exact={true} component={HOC(CourseRequirements)} />
          <Route path='/requirement' exact={true} component={HOC(Requirement)} />
          <Route path='/dashboard' exact={true} component={HOC(CourseDashboard)} />
          <Route path='/lecturers' exact={true} component={HOC(Lecturers)} />
          <Route path='/modules' exact={true} component={HOC(ChooseModule)} />
          <Route path='/admin/module' exact={true} component={HOC(AdminModule)} />

          <Route path='/admin/upload' exact={true} component={HOC(UploadCourse)} />
          <Route path='/lecturers/select-requirements' exact={true} component={HOC(SelectRequirements)} />
        </Switch>
      </Router>


    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    hideAlert: () => dispatch(hideAlert())
  };
};
export default connect(null, mapDispatchToProps)(App)


//Higher Order Component
const HOC = (WrappedComponent) => (props) => (
  <div>
    <AppNavbar />
    <div><WrappedComponent {...props} /></div>
  </div>
)

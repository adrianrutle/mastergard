import React from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

const column = (selectedCourse) => {
    return {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Personer tilknyttet ' + selectedCourse.title
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        colors: ["#e4d354", "#91e8e1", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"],
        tooltip: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Antall student / forelesere'
            }
        },
        series: [{
            "colorByPoint": true,
            data: [{
                "name": "Studenter",
                "y": selectedCourse.courseData.students
            },
            {
                "name": "Forelesere",
                "y": selectedCourse.courseData.lecturers
            },
            ]
        }]
    }

}
const ColumnChart = (props) => {
    return (
        <div>
            <HighchartsReact
                highcharts={Highcharts}
                options={column(props.data)}
            />
        </div>
    )
}
export default ColumnChart;

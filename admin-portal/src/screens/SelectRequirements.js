import React, { Component } from 'react'
import LoadingSpinner from '../components/LoadingSpinner';
import { connect } from 'react-redux'
import { Button, Container, Table, Input, FormGroup, Label } from 'reactstrap';
import { getRequirementForCourse, addCourseRequirementsToLecturer } from '../store/actions'

export class SelectRequirements extends Component {
    state = {
        selectedRequirements: [],
    }

    componentDidMount() {
        this.props.getRequirementForCourse()
        /* this.props.getAvailableLecturers() */
    }

    updateSelectedRequirement(requirementId) {
        let updatedArray = this.state.selectedRequirements
        if (this.state.selectedRequirements.find(id => id === requirementId) === undefined) {
            updatedArray.push(requirementId)
        } else {
            updatedArray.pop(requirementId)
        }
        this.setState({
            selectedRequirements: updatedArray
        })
    }

    populateRequirementsTable(requirementsData) {
        if (requirementsData.length > 0) {
            return requirementsData.map(requirement => {
                return <tr key={requirement.id}>
                    <td style={{ whiteSpace: 'nowrap' }}>{requirement.subject}</td>
                    <td style={{ whiteSpace: 'nowrap' }}>{requirement.title}</td>
                    <td>
                        <FormGroup check>
                            <Label check>
                                <Input
                                    type="checkbox"
                                    onClick={() => this.updateSelectedRequirement(requirement.id)} />
                                Velg
                            </Label>
                        </FormGroup>
                    </td>
                </tr>

            });
        }
    }

    render() {
        if (this.props.isLoading) {
            return (<LoadingSpinner />)
        }
        return (
            <div>
                <Container className={"App"}>
                    <h1 className={"header"}>Velg aktiviter {this.props.selectedLecturer.name} kan signere</h1>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th width="33%">Kategori</th>
                                <th width="33%">Tittel</th>
                                <th width="33%">Valg av aktivitet </th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.populateRequirementsTable(this.props.requirements)}
                        </tbody>
                    </Table>

                    <Button color="success"
                        className="successButton"
                        onClick={() => this.props.addCourseRequirementsToLecturer(this.state.selectedRequirements)}>
                        Fullfør!
                    </Button>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        requirements: state.requirements.requirements,
        selectedLecturer: state.lecturer.selectedLecturer,
        isLoading: state.ui.isLoading
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        addCourseRequirementsToLecturer: (requirements) => dispatch(addCourseRequirementsToLecturer(requirements)),
        getRequirementForCourse: (courseId) => dispatch(getRequirementForCourse(courseId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectRequirements)

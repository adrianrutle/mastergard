import React, { Component } from 'react'
import { connect } from "react-redux";
import { Text, View, Button } from 'react-native'
import { hideAlert, logout } from "../../store/actions";
import Dialog,
{
    DialogContent,
    DialogTitle,
    DialogFooter,
    ScaleAnimation,
    DialogButton
} from 'react-native-popup-dialog';
class AlertHandler extends Component {

    alertButtonAction = () => {
        if (this.props.alert.buttonFunction !== undefined) {
            this.props.hideAlert()
            this.props.logout()
        } else {
            this.props.hideAlert()
        }
    }
    render() {
        return (
            <Dialog

                width={0.9}
                visible={this.props.alert.visible}
                dialogAnimation={new ScaleAnimation()}
                dialogTitle={
                    <DialogTitle textStyle={{ fontSize: 20 }}
                        title={this.props.alert.title}
                        hasTitleBar={false}
                    />
                }
                footer={
                    <DialogFooter>
                        <DialogButton
                            text="OK"
                            onPress={this.alertButtonAction}
                        />
                    </DialogFooter>
                }
            >
                <DialogContent style={{ alignItems: "center" }}>
                    <Text style={{ fontSize: 17 }}>{this.props.alert.message}</Text>
                </DialogContent>
            </Dialog>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        alert: state.ui.alert
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideAlert: () => dispatch(hideAlert()),
        logout: () => dispatch(logout())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AlertHandler);

package com.regapp.regapp.repository;

import com.regapp.regapp.model.Lecturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LecturerRepository extends JpaRepository<Lecturer, Long> {
    Lecturer findLecturerByUserid(String userid);
    Lecturer findLecturerByEmail(String email);
    Lecturer findLecturerByName(String name);
}

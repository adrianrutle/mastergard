import React from "react";
import { TouchableOpacity, TouchableNativeFeedback, Platform, Text, View, StyleSheet } from "react-native";
import IconIonic from 'react-native-vector-icons/Ionicons';


const ButtonWithIcon = props => {

    const content = (
        <View style={[styles.button, props.style]}>
            <View style={styles.textContainer}>
                <Text style={styles.text}>{props.children}</Text>
            </View>
            <View style={styles.iconContainer}>
                <IconIonic
                    color={props.iconColor}
                    name={props.icon}
                    size={props.iconSize} />
            </View>


        </View>
    );
    if (Platform.OS === 'android') {
        return (
            <TouchableNativeFeedback onPress={props.onPress}>
                {content}
            </TouchableNativeFeedback>
        );
    }
    return <TouchableOpacity onPress={props.onPress}>{content}</TouchableOpacity>
};


const styles = StyleSheet.create({
    button: {
        flex: 1,
        marginBottom: 10,
        height: 50,
        borderWidth: 1,
        borderColor: '#f1f1f1',
        backgroundColor: '#fafafa',
        flexDirection: 'row',

        borderRadius: 4,
        borderBottomWidth: 0,
        shadowColor: '#f1f1f1',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.9,
        shadowRadius: 2,
        elevation: 1,

    },
    iconContainer: {
        flex: 1,
        justifyContent: 'center',
        //   paddingLeft: 20,
        //  paddingRight: 10
        alignItems: 'center'
    },
    textContainer: {
        flex: 10,
        justifyContent: 'center',
    },
    text: {
        textAlign: 'center',
        fontSize: 18
    }
});

export default ButtonWithIcon;
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux'
import { Platform } from 'react-native'
import Auth from "./src/screens/Auth/Auth";
import Feide from "./src/screens/Feide/Feide";
import LogoutScreen from "./src/screens/Logout/Logout";

import configureStore from './src/store/configureStore';

import HomeStudent from './src/screens/Student/HomeStudent/HomeStudent';
import RequirementStudent from './src/screens/Student/RequirementStudent/RequirementStudent';
import ScanQR from './src/screens/Student/ScanQR/ScanQR'
import SuccessScreen from './src/screens/Student/Success/Success'

import HomeLecturer from './src/screens/Lecturer/HomeLecturer/HomeLecturer';
import RequirementLecturerScreen from './src/screens/Lecturer/RequirementLecturer/RequirementLecturer';
import GenerateQRScreen from './src/screens/Lecturer/GenerateQR/GenerateQR';

//Disable the Yellow Box in React Native
//console.disableYellowBox = true;

const store = configureStore();

// Register Screens
Navigation.registerComponent(
    "regapp.AuthScreen",
    () => Auth,
    store,
    Provider);

Navigation.registerComponent(
    "regapp.LogoutScreen",
    () => LogoutScreen,
    store,
    Provider);

Navigation.registerComponent(
    "regapp.FeideScreen",
    () => Feide, store,
    Provider);


// STUDENT
Navigation.registerComponent(
    "regapp.HomeScreenStudent",
    () => HomeStudent,
    store,
    Provider);

Navigation.registerComponent(
    "regapp.RequirementStudentScreen",
    () => RequirementStudent,
    store,
    Provider);

Navigation.registerComponent(
    "regapp.ScanQR",
    () => ScanQR,
    store,
    Provider);

Navigation.registerComponent(
    "regapp.SuccessScreen",
    () => SuccessScreen);

// LECTURER
Navigation.registerComponent(
    "regapp.HomeScreenLecturer",
    () => HomeLecturer,
    store,
    Provider);

Navigation.registerComponent(
    "regapp.RequirementLecturerScreen",
    () => RequirementLecturerScreen,
    store,
    Provider);

Navigation.registerComponent(
    "regapp.GenerateQRScreen",
    () => GenerateQRScreen,
    store,
    Provider);

//Start a App
Navigation.startSingleScreenApp({
    screen: {
        screen: "regapp.AuthScreen"
    },
    appStyle: {
        navBarHidden: Platform.OS === 'android' ? false : true,
        hideBackButtonTitle: true,
        navBarButtonColor: 'white',
        navBarBackgroundColor: '#80deea',
        statusBarColor: Platform.OS === 'android'
            ? '#4bacb8' : undefined,
        navBarTextColor: 'white'
    }
});
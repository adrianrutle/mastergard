import React, { Component } from 'react';
import { StyleSheet, View, Animated, TouchableHighlight, Text } from "react-native";
import IconIonic from "react-native-vector-icons/Ionicons"
import { Divider } from "react-native-elements";

class panel extends Component {

    state = {
        expanded: false,
        animation: new Animated.Value()
    }


    componentWillMount() {
        this.state.animation.setValue(this.props.panelHeaderSize);
    }

    generateContainerSize() {
        return this.props.panelHeaderSize + 50 + (this.props.panelContainerSize * this.props.numberOfTasks)
    }

    toggle() {
        let initialValue = this.state.expanded ? this.generateContainerSize() : this.props.panelHeaderSize,
            finalValue = this.state.expanded ? this.props.panelHeaderSize : this.generateContainerSize();
        this.setState({
            expanded: !this.state.expanded
        });

        this.state.animation.setValue(initialValue);

        Animated.spring(
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }

    render() {
        var iconId = 'ios-school';

        if (this.state.expanded) {
            iconId = 'md-school'
        }
        return (
            <Animated.View
                style={[styles.container, { height: this.state.animation }]}>

                <TouchableHighlight
                    onPress={() => this.toggle()}
                    underlayColor="#f1f1f1">

                    <View style={styles.headerContainer}>
                        <Text style={styles.headerTitle}>{this.props.title}</Text>
                        <Text style={styles.headerItem}>{this.props.requirementCount}</Text>
                        <IconIonic style={styles.headerItem} color={'#48acb8'} name={'ios-book-outline'} size={30} />

                    </View>

                </TouchableHighlight>

                <View style={styles.body}>
                    <Divider style={{ backgroundColor: '#d3d3d3' }} />

                    {this.props.children}
                </View>


            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        margin: 10,
        overflow: 'hidden',
        borderRadius: 20
    },
    headerContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        height: 50
    },
    headerTitle: {
        flex: 1,
        padding: 10,
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20
    },
    headerItem: {
        padding: 5
    },
    body: {
        paddingLeft: 10,
        paddingRight: 10,
        flex: 1,

    }
});

export default panel;
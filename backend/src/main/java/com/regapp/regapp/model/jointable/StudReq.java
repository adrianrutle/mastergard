package com.regapp.regapp.model.jointable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Entity
@Table(name = "STUDENT_REQUIREMENTS")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.student",
                joinColumns = @JoinColumn(name = "STUDENT_ID")),
        @AssociationOverride(name = "primaryKey.requirement",
                joinColumns = @JoinColumn(name = "REQUIREMENT_ID"))
})
public class StudReq {

    @EmbeddedId
    @JsonBackReference(value="student-req")
    private StudReqId primaryKey = new StudReqId();
    private Boolean signed;
    private String date;
    private String studentGroup;

    private String signedTimestamp;


    public StudReqId getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(StudReqId primaryKey) {
        this.primaryKey = primaryKey;
    }

    @JsonBackReference
    @Transient //Hibernate doesn’t try to map the getter
    public Student getStudent() {
        return getPrimaryKey().getStudent();
    }

    public void setStudent(Student student) {
        getPrimaryKey().setStudent(student);
    }

    @Transient
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    @Transient
    public String getStudentGroup() {
        return studentGroup;
    }

    public void setStudentGroup(String studentGroup) {
        this.studentGroup = studentGroup;
    }

    @Transient //Hibernate doesn’t try to map the getters.
    public Requirement getRequirement() {
        return getPrimaryKey().getRequirement();
    }

    public void setRequirement(Requirement requirement) {
        getPrimaryKey().setRequirement(requirement);
    }

    public void setRequirements(List<Requirement> requirements) {
        for (Requirement req : requirements) {
            getPrimaryKey().setRequirement(req);
        }

    }

    public String getSignedTimestamp() {
        return signedTimestamp;
    }

    public void setSignedTimestamp() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        formatter.format(LocalDateTime.now());
        this.signedTimestamp =  formatter.format(LocalDateTime.now());
    }

    public Boolean isRequirementSigned() {
        return signed;
    }

    public void signRequirement(Boolean signed) {
        this.signed = signed;
    }
}

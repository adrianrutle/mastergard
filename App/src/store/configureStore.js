import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'

import uiReducer from './reducers/ui';
import authReducer from './reducers/auth';
import coursesReducer from './reducers/courses';
import userReducer from './reducers/user';

const rootReducer = combineReducers({
    ui: uiReducer,
    auth: authReducer,
    courses: coursesReducer,
    user: userReducer
});

let componseEnhancers = compose;

if (__DEV__) {
    componseEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = () => {
    return createStore(rootReducer, componseEnhancers(applyMiddleware(thunk)));
};

export default configureStore;
import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Text, ActivityIndicator } from 'react-native'
import { Card, Divider } from "react-native-elements";
import { connect } from "react-redux";

import QRCode from 'react-native-qrcode';
import MainText from '../../../components/UI/MainText/MainText';
import { getQRCodeString } from '../../../store/actions';


class GenerateQRScreen extends Component {
    state = {
        qrString: undefined
    };

    componentDidMount() {
        this.props.getQRCodeString(this.props.requirementId).then((qrString) => {
            if (qrString !== undefined) {
                this.setState({ qrString: qrString });
            }

        })

        console.log('STATE: ' + this.state.qrString);
    }

    renderQr() {
        if (this.state.qrString === undefined) {
            return (
                <ActivityIndicator size="large" color="#4bacb8" />
            )
        } else {
            return (
                <QRCode
                    value={this.state.qrString}
                    size={250}
                    bgColor='black'
                    fgColor='white' />
            )
        }
    }

    render() {
        return (
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.container}>
                    <Card containerStyle={styles.cardContainer}>
                        <View style={styles.textContainer}>
                            <MainText style={{ fontSize: 30 }}>{this.props.requirementTitle}</MainText>
                            <MainText style={{ fontSize: 20 }}>Representerer: {this.props.lecturer.name} </MainText>
                            <Divider style={{ backgroundColor: '#d3d3d3' }} />
                        </View>

                        <View style={styles.qrContainer}>

                            {this.renderQr()}

                        </View>

                    </Card>
                </View>
            </ScrollView>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#b4ffff',

    },
    cardContainer: {
        flex: 1,
        borderColor: "#ddd",
        margin: 20,
        marginTop: 40,
        marginBottom: 40,

        justifyContent: 'center',
        backgroundColor: '#fff',
        borderColor: '#fff',
        borderRadius: 20
    },

    textContainer: {
        flex: 1,
        marginBottom: 20
    },
    qrContainer: {
        marginTop: 40,
        flex: 3,
        alignItems: 'center',
        marginBottom: 40
    }
});

const mapStateToProps = state => {
    return {
        lecturer: state.user.lecturer
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getQRCodeString: (requirementId) => dispatch(getQRCodeString(requirementId)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(GenerateQRScreen);
package com.regapp.regapp.service.Course;

import com.regapp.regapp.exception.InvalidDataException;
import com.regapp.regapp.exception.ResourceNotFoundException;
import com.regapp.regapp.model.Course;
import com.regapp.regapp.model.Lecturer;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.repository.CourseRepository;
import com.regapp.regapp.repository.LecturerRepository;
import com.regapp.regapp.repository.RequirementRepository;
import com.regapp.regapp.repository.StudentRepository;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("CourseService")
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    RequirementRepository requirementRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    LecturerRepository lecturerRepository;

    @Override
    public List<Course> getCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Course addCourse(String courseTitle, List<Requirement> requirements, List<Lecturer> lecturers, List<StudReq> studReqs) {
        if (courseRepository.findCourseByTitle(courseTitle) == null) {
            Course course = new Course(courseTitle);

            for (Lecturer lec : lecturers) {
                Lecturer existingLecturer = lecturerRepository.findLecturerByEmail(lec.getEmail());
                if (existingLecturer == null) {
                    lec.addRequirements(requirements);
                    lecturerRepository.save(lec);
                } else {
                    existingLecturer.addRequirements(requirements);
                    lecturerRepository.save(existingLecturer);
                }

            }
            course.addRequirements(requirements);
            courseRepository.save(course);

            for (Requirement courseRequirement : course.getRequirements()) {
                for (StudReq studReq : studReqs) {
                    if (courseRequirement.getTitle().equals(studReq.getRequirement().getTitle())) {
                        Student student = studReq.getStudent();
                                Student existingstudent = studentRepository.findStudentByEmail(student.getEmail());
                                studReq.setRequirement(courseRequirement);
                                if (existingstudent == null) {
                                    studReq.setStudent(student);
                                    student.addStudRequirement(studReq);
                                    studentRepository.save(student);
                                } else {
                                    studReq.setStudent(existingstudent);
                                    existingstudent.addStudRequirement(studReq);
                                    studentRepository.save(existingstudent);
                                }
                            }}
            }

            return course;
        } else {
            throw new InvalidDataException(courseTitle);

        }

    }

    @Override
    public Course getCourseById(Long courseId) {
        return courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
    }

    @Override
    public Course updateCourse(Long courseId, Course courseDetails) {
        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
        course.setTitle(courseDetails.getTitle());

        Course updatedCourse = courseRepository.save(course);
        return updatedCourse;
    }

    @Override
    public List<Requirement> getCourseRequirements(Long courseId) {
        Course c = courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
        return c.getRequirements();

    }

    @Override
    public List<StudReq> getCourseRequirementsForStudent(Long courseId, Long studentId) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student", "studentId", studentId));

        List<StudReq> studReqs = new ArrayList<>();
        for (StudReq sr : student.getStudReqs()) {
            Requirement r = sr.getRequirement();
            if (r.getCourse().getId().equals(courseId)) {
                studReqs.add(sr);
            }
        }
        return studReqs;
    }

    @Override
    public List<Requirement> getCourseRequirementsForLecturer(Long courseId, Long lecturerId) {
        Course c = courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
        List<Requirement> courseRequirments = c.getRequirements();
        Lecturer lecturer = lecturerRepository.findById(lecturerId)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));

        List<Requirement> lecturerRequirments = lecturer.getRequirements();

        List<Requirement> requirementsForThisLecturer = new ArrayList<>();
        for (Requirement courseRequirement : courseRequirments) {
            if (lecturerRequirments.contains(courseRequirement)) {
                requirementsForThisLecturer.add(courseRequirement);
            }
        }
        return requirementsForThisLecturer;
    }

    @Override
    public List<Student> getCourseStudents(Long courseId) {
        Course c = courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
        List<Student> allStudents = new ArrayList<>();

        for (Requirement r : c.getRequirements()) {
            for (Student s : r.getAllStudents()) {
                if (!allStudents.contains(s)) {
                    allStudents.add(s);
                }
            }
        }
        return allStudents;
    }

    @Override
    public List<Lecturer> getCourseLecturers(Long courseId) {
        Course c = courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
        List<Lecturer> allLecturers = new ArrayList<>();

        for (Requirement r : c.getRequirements()) {
            for (Lecturer l : r.getAllLecturers()) {
                if (!allLecturers.contains(l)) {
                    allLecturers.add(l);
                }
            }
        }
        return allLecturers;
    }

    @Override
    public void addRequirementToCourse(Long courseId, Requirement requirement) {
        Course c = courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
        c.addRequirement(requirement);
        courseRepository.save(c);
    }

    @Override
    public void deleteRequirementFromCourse(Long courseId, Long requirementId) {

        Course c = courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
        Requirement requirement = requirementRepository.findById(requirementId)
                .orElseThrow(() -> new ResourceNotFoundException("Requirement", "requirementID", requirementId));
        c.removeRequirement(requirement);
        courseRepository.save(c);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course c = courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course", "courseId", courseId));
        courseRepository.delete(c);
    }

    @Override
    public Lecturer removeLecturerFromCourse(Long courseId, Long lecturerId) {
        Lecturer lecturer = lecturerRepository.findById(lecturerId)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer", "lecturerId", lecturerId));
        lecturer.removeLecturerFromCourse(courseId);
        Lecturer updatedLecturer = lecturerRepository.save(lecturer);
        return updatedLecturer;
    }
}

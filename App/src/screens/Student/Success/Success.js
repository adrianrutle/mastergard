import React, { Component } from 'react';
import { ScrollView, View, StyleSheet } from 'react-native'
import { Button } from 'react-native-elements';
import HeadingText from '../../../components/UI/HeadingText/HeadingText'
import IconIonic from 'react-native-vector-icons/Ionicons';
class SuccessScreen extends Component {

    goBack() {
        this.props.navigator.popToRoot({
            animated: true, // does the popToRoot have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the popToRoot have different transition animation (optional)
        });
    }

    render() {
        return (
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.container}>
                    <HeadingText style={styles.header}>Aktivitet er signert!</HeadingText>
                    <View style={styles.imageContainer}>
                        <IconIonic color={'#4cd964'}
                            name={'ios-checkmark-circle-outline'}
                            size={250} />


                    </View>
                    <View style={styles.buttonContainer}>
                        <Button
                            onPress={() => this.goBack()}
                            title='Gå tilbake' />

                    </View>

                </View >

            </ScrollView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#edf0f5',
        justifyContent: 'center',
    },
    imageContainer: {
        marginBottom: '10%',
        alignItems: 'center'
    },
    header: {
        marginBottom: '10%',
        fontSize: 38
    }
});
export default SuccessScreen;
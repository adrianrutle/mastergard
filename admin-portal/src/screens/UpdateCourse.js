import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoadingSpinner from '../components/LoadingSpinner';

import { showError, updateCourseInfo } from '../store/actions'
import {
    Container,
    Form, FormGroup,
    Label, Input, Button
} from 'reactstrap';

export class UpdateCourse extends Component {
    state = {
        courseId: "",
        courseTitle: "",
        courseInfo: ""
    }

    componentDidMount() {
        this.populateForm()
    }
    populateForm() {
        var selectedCourse = this.props.selectedCourse;
        this.setState(state => {
            return {
                ...state,
                courseId: selectedCourse.id,
                courseTitle: selectedCourse.title
            }

        })
    }

    onFormSubmit = (e) => {
        e.preventDefault()
        this.props.updateCourseInfo(this.state)
    }

    render() {
        if (this.props.isLoading) {
            return (<LoadingSpinner />)
        }

        return (
            <Container>
                <h1 className={"header"}>Oppdater Emne</h1>
                <div className={"Form"}>
                    <Form onSubmit={this.onFormSubmit}>
                        <FormGroup >
                            <Label for="courseTitle">Tittel på emne</Label>
                            <Input name="title"
                                id="courseTitle"
                                value={this.state.courseTitle}
                                onChange={e => this.setState({ courseTitle: e.target.value })}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="courseInfo">emnebeskrivelse</Label>
                            <Input type="textarea" name="text" id="courseInfo" />
                        </FormGroup>
                        <Button type="submit">Lagre</Button>
                    </Form>
                </div>
            </Container>


        )
    }
}



const mapStateToProps = (state) => {
    return {
        selectedCourse: state.courses.selectedCourse,
        isLoading: state.ui.isLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        showError: (errorMessage) => dispatch(showError(errorMessage)),
        updateCourseInfo: (courseInfo) => dispatch(updateCourseInfo(courseInfo))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateCourse)



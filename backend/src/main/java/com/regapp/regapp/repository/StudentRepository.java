package com.regapp.regapp.repository;

import com.regapp.regapp.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findStudentByUserid(String userid);
    Student findStudentByEmail(String email);
    Student findStudentByName(String name);
}

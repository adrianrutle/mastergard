package com.regapp.regapp.service.Admin;

public interface AdminService {
    Boolean checkIfUserIsAdmin(String email);
}

import { getToken } from './token'
import { appConfig } from '../util'
import { getStudent } from './student'
import { getLecturer } from './lecturer'
import { SET_REQUIREMENTS } from "./actionTypes";
import { uiStartLoading, uiStopLoading, showAlertWithFunc } from "./ui";

/**
 * Student
 */
export const getCourseRequirements = (courseId) => {
    return dispatch => {
        dispatch(uiStartLoading())
        dispatch(getStudent()).then((student) => {
            dispatch(getToken())
                .then(token => {
                    fetch(appConfig.backendUrl + 'course/student/requirement/' + courseId + '/' + student.id, {
                        method: 'GET',
                        headers: {
                            "Content-type": "application/json",
                            "Authorization": "Bearer " + token
                        }
                    }).catch(err => {
                        console.log(err);
                        dispatch(uiStopLoading())
                    })
                        .then(res => {
                            if (res.status === 200) {
                                res.json().then(requirements => {
                                    dispatch(setRequirements(structureStudentRequirements(requirements)));
                                    dispatch(uiStopLoading())
                                });
                            } else {
                                dispatch(showAlertWithFunc(
                                    "Advarsel",
                                    "Det skjedde noe galt, vennligst logg inn på nytt",
                                    "LOGOUT"
                                ))
                                dispatch(uiStopLoading())
                            }

                        })

                });
        })
    }
}


/**
 * Lecturer
 */
export const getCourseLecturerRequirements = (courseId) => {
    return dispatch => {
        dispatch(uiStartLoading())
        dispatch(getLecturer()).then((lecturer) => {
            dispatch(getToken())
                .then(token => {
                    fetch(appConfig.backendUrl + 'course/lecturer/requirement/' + courseId + '/' + lecturer.id, {
                        method: 'GET',
                        headers: {
                            "Content-type": "application/json",
                            "Authorization": "Bearer " + token
                        }
                    }).catch(err => {
                        console.log(err);
                        dispatch(uiStopLoading())
                    })
                        .then(res => {
                            if (res.status === 200) {
                                res.json().then(requirements => {
                                    dispatch(setRequirements(structureLecturerRequirements(requirements)));
                                    dispatch(uiStopLoading())
                                })
                            } else {
                                dispatch(showAlertWithFunc(
                                    "Advarsel",
                                    "Det skjedde noe galt, vennligst logg inn på nytt",
                                    "LOGOUT"
                                ))
                                dispatch(uiStopLoading())
                            }
                        })
                });
        })
    }
}


/**
 * 
 * Refactor to lecturer actions
 */
const structureLecturerRequirements = (requirements) => {
    console.log(requirements);
    var structuredRequirement = [];
    var addRequirement = true;
    requirements.forEach(requirement => {
        structuredRequirement.find(x => {
            if (x.subject === requirement.subject) {
                let extraSubjectRequirement = {
                    id: requirement.id,
                    title: requirement.title
                }
                x.tasks.push(extraSubjectRequirement)
                addRequirement = false;
            }
        });
        if (addRequirement) {
            structuredRequirement.push({
                subject: requirement.subject,
                tasks: [{
                    id: requirement.id,
                    title: requirement.title
                }]
            })
            addRequirement = true;
        }
    })
    return structuredRequirement
}

/**
 * 
 * Refactor to student actions
 */
const structureStudentRequirements = (requirements) => {
    var structuredRequirement = [];
    var addRequirement = true;
    requirements.forEach(requirementInfo => {
        structuredRequirement.find(x => {
            if (x.subject === requirementInfo.requirement.subject) {
                let extraSubjectRequirement = {
                    id: requirementInfo.requirement.id,
                    title: requirementInfo.requirement.title,
                    date: requirementInfo.date,
                    studentGroup: requirementInfo.studentGroup,
                    signed: requirementInfo.requirementSigned
                }
                x.numberOfSignedTasks = requirementInfo.requirementSigned ? x.numberOfSignedTasks + 1 : x.numberOfSignedTasks
                x.tasks.push(extraSubjectRequirement)
                addRequirement = false;
            }
        });
        if (addRequirement) {
            structuredRequirement.push({
                subject: requirementInfo.requirement.subject,
                numberOfSignedTasks: requirementInfo.requirementSigned ? 1 : 0,
                tasks: [{
                    id: requirementInfo.requirement.id,
                    title: requirementInfo.requirement.title,
                    date: requirementInfo.date,
                    studentGroup: requirementInfo.studentGroup,
                    signed: requirementInfo.requirementSigned
                }]
            })
            addRequirement = true;
        }
    })
    return structuredRequirement
}


export const signRequirement = (qrString, requirementId) => {
    return dispatch => {
        dispatch(uiStartLoading())
        const promise = new Promise((resolve, reject) => {

            dispatch(getStudent()).then((student) => {
                dispatch(getToken())
                    .then(token => {
                        fetch(appConfig.backendUrl +
                            'lecturer/requirement/sign', {
                                method: 'PUT',
                                body: JSON.stringify({
                                    "qrString": qrString,
                                    "studentId": student.id,
                                    "requirementId": requirementId
                                }),
                                headers: {
                                    "Content-type": "application/json",
                                    "Authorization": "Bearer " + token
                                }
                            }).catch(err => {
                                console.log(err);
                                dispatch(uiStopLoading())
                                reject("Det er noen problemer med systemet, prøv igjen senere ");
                            })
                            .then(res => {
                                if (res.status === 404 || res.status === 400) {
                                    dispatch(uiStopLoading())
                                    return reject("Ugyldig QR-kode");

                                }
                                if (res.status === 422) {
                                    dispatch(uiStopLoading())
                                    return reject("Ugyldig signatur!");

                                }
                                res.json().then((requirementInfo) => {

                                    dispatch(updateRequirementSignStatus(requirementInfo.requirement)).then(() => {
                                        dispatch(uiStopLoading())
                                        resolve();
                                    })
                                })
                            });
                    });
            })
        });
        promise.catch(err => {
            console.log('err:' + err);
        });
        return promise;
    }
}

const updateRequirementSignStatus = (requirement) => {
    return (dispatch, getState) => {
        const promise = new Promise((resolve, reject) => {
            const requirements = getState().courses.requirements;
            requirements.forEach(reduxRequirement => {
                if (reduxRequirement.subject === requirement.subject) {
                    reduxRequirement.tasks.forEach(task => {
                        if (task.id === requirement.id) {
                            task.signed = true;
                            reduxRequirement.numberOfSignedTasks = reduxRequirement.numberOfSignedTasks + 1;
                            dispatch(setRequirements(requirements));
                            resolve();
                        }
                    });
                }
            });
            resolve()
        })
        promise.catch(err => {
            console.log('err:' + err);

        });
        return promise;
    }
}


export const getQRCodeString = (requirementId) => {

    return dispatch => {
        const promise = new Promise((resolve, reject) => {
            dispatch(getLecturer()).then((lecturer) => {
                dispatch(getToken())
                    .then(token => {
                        fetch(appConfig.backendUrl + 'lecturer/requirement/signature/' + lecturer.id + '/' + requirementId, {
                            method: 'GET',
                            headers: {
                                "Authorization": "Bearer " + token
                            }
                        }).catch(err => {
                            console.log(err);
                            reject()

                        }).then(res => {
                            console.log(res._bodyText);
                            resolve(res._bodyText)

                        })
                    })
            })
        })
        promise.catch(err => {
            console.log('err:' + err);
        });
        return promise;
    }

}

export const setRequirements = requirements => {
    return {
        type: SET_REQUIREMENTS,
        requirements: requirements
    }
}
export const SHOW_ERROR = 'SHOW_ERROR';
export const SHOW_INFO = "SHOW_INFO";
export const HIDE_ALERT = "HIDE_ALERT";
export const SHOW_SUCCESS = "SHOW_SUCCESS";

export const UI_START_LOADING = "UI_START_LOADING";
export const UI_STOP_LOADING = "UI_STOP_LOADING";

export const SET_COURSES = "SET_COURSES";
export const REMOVE_COURSES = "REMOVE_COURSES";
export const SELECTED_COURSE = "SELECTED_COURSE";
export const SELECTED_COURSE_DATA = "SELECTED_COURSE_DATA";


export const SET_COURSE_LECTURERS = "SET_COURSE_LECTURERS";
export const SET_AVAILABLE_LECTURERS = "SET_AVAILABLE_LECTURERS";
export const SELECTED_LECTURER = "SELECTED_LECTURER";

export const SET_REQUIREMENTS = "SET_REQUIREMENTS";
export const REMOVE_REQUIREMENTS = "REMOVE_REQUIREMENTS";
export const SELECTED_REQUIREMENT = "SELECTED_REQUIREMENT";
export const SELECTED_REQUIREMENT_DATA = "SELECTED_REQUIREMENT_DATA";



export const REMOVE_LECTURERS = "REMOVE_LECTURERS";

export const AUTHORIZE = "AUTHORIZE";
export const LOGOUT = "LOGOUT";





package com.regapp.regapp.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class controller {

    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }
    @RequestMapping(value = "/privacy-policy.html")
    public String privacyPolicy() {
        return "privacy-policy.html";
    }
    @RequestMapping(value = "/support.html")
    public String support() {
        return "support.html";
    }
    @RequestMapping(value = "/oauth-redirect.html")
    public String oauthRedirect() {
        return "oauth-redirect.html";
    }
}

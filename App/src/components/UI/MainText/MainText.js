import React from "react";
import { Text, StyleSheet } from "react-native";

const mainText = props => (
    <Text style={[styles.mainText, props.style]}>
        {props.children}
    </Text>
);

const styles = StyleSheet.create({
    mainText: {
        marginTop: 2,
        marginBottom: 2,
        color: "black",
        backgroundColor: "transparent",
        textAlign: 'center'
    },
});

export default mainText;
package com.regapp.regapp.utils;

import com.regapp.regapp.model.*;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
@Service
public class DBSeeder2  implements CommandLineRunner {


    @Autowired
    private LecturerRepository lecturerRepository;

    @Autowired
    private RequirementRepository requirementRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudReqRepository studReqRepository;

    @Autowired
    private AdminRepository adminRepository;
    @Override
    @Transactional
    public void run(String... args) throws Exception {
       // lecturerRepository.deleteAll();
       // requirementRepository.deleteAll();
       // courseRepository.deleteAll();
       // studentRepository.deleteAll();
       // studReqRepository.deleteAll();
        adminRepository.deleteAll();
        Lecturer frank = new Lecturer("Frank Foreleser Føllesen","frank_foreleser@spusers.feide.no");

        Lecturer test1 = new Lecturer("Trym Olsen ","sdf@asdfhsd.no");
        Lecturer test2 = new Lecturer("Ola Johansen","sdfhrhqhy@wrgr.no");
        Lecturer test3 = new Lecturer("Turi Amunsen","hwfh@wergre.no");


        Admin admin = new Admin("Gard","gen008@uib.no");
        adminRepository.save(admin);

        Student student = new Student("Gard","575532@hvl.no");
 //       studentRepository.save(student);
        Course MED7 = new Course("MED7");
        Course MED8 = new Course("MED8");

        Requirement requirement = new Requirement(
                "Nevrologi",
                "Tutorgruppe 1",
                "signature");

        Requirement requirement1 = new Requirement(
                "Sirkulasjon 1",
                "Ultralyd the real deal",
                "signature");
        Requirement requirement2 = new Requirement(
                "Sirkulasjon 1",
                "Ultralyd eksamen",
                "signature");

        Requirement requirement3 = new Requirement(
                "Psykogreie",
                "eksamentest",
                "signature");

        Requirement requirement4 = new Requirement(
                "Psykogreie",
                "eksamentest nummer 2",
                "signature");

     //   lecturerRepository.save(frank);
       /*
        frank.addRequirement(requirement);
        MED7.addRequirement(requirement);
        MED7.addRequirement(requirement1);
        MED7.addRequirement(requirement2);
        MED7.addRequirement(requirement3);
        MED7.addRequirement(requirement4);



        lecturerRepository.save(test1);
        lecturerRepository.save(test2);
        lecturerRepository.save(test3); */

      //  courseRepository.save(MED7);
      //  courseRepository.save(MED8);


        /*
        Student gard = new Student(
                "Gard Aaland Engen",
                "575532@stud.hvl.no"
        );
        Student gard2 = new Student(
                "Gard Aaland Engen",
                "Gard.Engen@student.uib.no"
        );
        StudReq studReq = new StudReq();
        //Req1
        studReq.setRequirement(requirement);
        studReq.setStudent(gard);
        studReq.signRequirement(false);
        gard.addStudRequirement(studReq);
        studentRepository.save(gard);

        studReq = new StudReq();
        studReq.setRequirement(requirement);
        studReq.setStudent(gard2);
        studReq.signRequirement(false);
        gard2.addStudRequirement(studReq);
        studentRepository.save(gard2); */
    }
}

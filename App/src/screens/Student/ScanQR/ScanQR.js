import React, { Component } from 'react';
import {
    StyleSheet,
    Alert,
    View,
    ScrollView,
    RefreshControl
} from 'react-native';
import { signRequirement } from '../../../store/actions';
import { connect } from "react-redux";
import QRCodeScanner from 'react-native-qrcode-scanner';
import { Divider, Card } from 'react-native-elements';
import HeadingText from '../../../components/UI/HeadingText/HeadingText'
import MainText from '../../../components/UI/MainText/MainText';

class ScanQRScreen extends Component {

    onSuccess(e) {
        this.props.signRequirement(e.data, this.props.requirementId).then(() => {
            this.props.navigator.push({
                screen: "regapp.SuccessScreen",
                title: "Sign success",
            });

        }).catch(errorString => {
            Alert.alert(
                'Feilmelding',
                errorString,
                [{ text: 'OK', onPress: () => this.scanner.reactivate() }],
                { cancelable: false }
            )
        })
    }
    render() {


        return (
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}
                refreshControl={
                    <RefreshControl
                        tintColor={"#80deea"}
                        refreshing={this.props.isLoading}
                    />}>
                <View style={styles.container}>
                    <Card containerStyle={styles.cardContainer}>
                        <View style={styles.textContainer}>
                            <MainText style={{ fontSize: 30 }}>Scan QR-koden som representerer aktiviteten</MainText>
                            <Divider style={{ backgroundColor: '#d3d3d3' }} />
                        </View>
                        <View style={styles.qrScanner}>

                            <QRCodeScanner
                                cameraStyle={{ width: '100%' }}
                                onRead={event => { this.onSuccess(event) }}
                                showMarker
                                customMarker={<View style={{ borderWidth: 2, borderColor: 'white', width: 200, height: 200 }}></View>}
                                ref={(node) => { this.scanner = node }}

                            />

                        </View>
                    </Card>
                </View>
            </ScrollView>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#b4ffff',
    },
    cardContainer: {
        flex: 1,
        borderColor: "#ddd",
        margin: 20,
        marginTop: 40,
        marginBottom: 40,

        justifyContent: 'center',
        backgroundColor: '#fff',
        borderColor: '#fff',
        borderRadius: 20
    },
    textContainer: {
        flex: 1,
        marginBottom: 20
    },
    qrScanner: {
        //  flex: 3
    }
});

const mapStateToProps = state => {
    return {
        isLoading: state.ui.isLoading
    };
};
const mapDispatchToProps = dispatch => {
    return {
        signRequirement: (qrString, requirementId) => dispatch(signRequirement(qrString, requirementId)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ScanQRScreen);
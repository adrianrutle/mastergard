import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, DropdownToggle, UncontrolledDropdown, NavItem, NavLink, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import Alert from './Alert';
import { connect } from 'react-redux';
import { doLogout } from '../store/actions';
import { hideAlert } from '../store/actions/ui'
class AppNavbar extends Component {

    authenticated() {
        if (this.props.isAuthenticated)
            return (
                <Nav className="ml-auto" navbar>

                    <NavItem>
                        <NavLink href="/modules">Moduler</NavLink>
                    </NavItem>

                    <NavItem>
                        <NavLink href="/courses">Emner</NavLink>
                    </NavItem>

                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret  >
                            Gard Engen
                </DropdownToggle>
                        <DropdownMenu right >
                            <DropdownItem onClick={() => this.props.onLogout()}>
                                Logg ut
                </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
            )
    }

    render() {
        return (
            <div>
                <Navbar color="dark" dark expand="md">
                    <NavbarBrand tag={Link} to="#">RegApp</NavbarBrand>
                    {this.authenticated()}

                </Navbar>
                <Alert color={this.props.ui.alertColor}
                    alertMessage={this.props.ui.alertMessage}

                    showAlert={this.props.ui.showAlert}></Alert>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        isAuthenticated: state.auth.isAuthenticated
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onLogout: () => dispatch(doLogout()),
        hideAlert: () => dispatch(hideAlert())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AppNavbar);
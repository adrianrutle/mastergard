import { AUTH_SET_AFFILIATION, AUTH_REMOVE_AFFILIATION } from "./actionTypes";
import { uiStartLoading, uiStopLoading, showAlert, showAlertWithFunc } from "./ui";
import { Navigation } from "react-native-navigation";
import { authorize } from 'react-native-app-auth';
import { fetchStudent, getStudent, removeStudent } from './student'
import { appConfig, clearStorage, storeItemInStorage, getItemFromStorage } from '../util'
import { getToken, storeToken, removeToken } from './token'
import { fetchLecturer, getLecturer, removeLecturer } from "./lecturer";
import { removeCourses } from './course'
import startMainTabs from '../../screens/MainTabs/startMainTabs'


export const handleDeepLinking = ({ params: { token } }) => {
    return dispatch => {
        Navigation.startSingleScreenApp({
            screen: {
                screen: "regapp.AuthScreen"
            },
            passProps: {
                accessToken: token
            },
            appStyle: {
                navBarHidden: true
            },
        });
    }
}

/**
 * Do feide login and store token
 */
export const doAuth = (accessToken) => {
    return dispatch => {
        dispatch(uiStartLoading())
        dispatch(storeToken(accessToken))
        fetch(appConfig.backendUrl + 'role', {
            method: 'GET',
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + accessToken
            }
        })
            .catch(err => {
                console.log(err);
                dispatch(uiStopLoading())
                dispatch(showAlert("Feilmelding", "Autorisering feilet"))
            })
            .then(res => res.json())
            .then(response => {
                dispatch(storeAffiliation(response[0].authority))
                if (response[0].authority === "ROLE_STUDENT") {
                    dispatch(fetchStudent(accessToken))

                } else if (response[0].authority === "ROLE_LECTURER") {
                    console.log('jeg er en lecturer');
                    dispatch(fetchLecturer(accessToken))
                } else if (response[0].authority === "ROLE_ADMIN") {
                    //Admin
                }

            }).catch(() => {
                dispatch(uiStopLoading())
                dispatch(showAlert("Feilmelding", "Du har ikke tilgang til RegApp"))
            })
    }
}


/**
 * This function will store affiliation information about 
 * the user in local storage.
 * @param {Store affiliation information in local storage} affiliation 
 */
export const storeAffiliation = affiliation => {
    return dispatch => {
        dispatch(setAffiliation(affiliation));
        storeItemInStorage('auth:affiliation', affiliation)
    }
}

export const getAffiliation = () => {
    return (dispatch, getState) => {
        const promise = new Promise((resolve, reject) => {
            const affiliation = getState().auth.affiliation;
            if (!affiliation) { // if affiliation is missing in redux, check local storage
                getItemFromStorage('auth:affiliation')
                    .catch(err => reject())
                    .then(affiliationFromStorage => {
                        if (!affiliationFromStorage) {
                            reject();
                            return;
                        }
                        dispatch(setAffiliation(affiliationFromStorage));
                        resolve(affiliationFromStorage);
                    })
            } else {
                resolve(affiliation);
            }

        });
        promise.catch(err => {
            dispatch(clearStorage())
        });
        return promise;
    };
}


/**
 * This function will check if the user has a valid access token stored
 * in local storage, if success, then it will redirect user to home-screen
 */
export const autoSignIn = () => {
    return dispatch => {
        dispatch(getToken())
            .then(() => {
                dispatch(getAffiliation())
                    .then(affiliation => {
                        if (affiliation === "ROLE_STUDENT") {
                            dispatch(getStudent())
                                .then(() => {
                                    startMainTabs(affiliation)
                                })
                        } else if (affiliation === "ROLE_LECTURER") {
                            dispatch(getLecturer())
                                .then(() => {
                                    startMainTabs(affiliation)
                                })
                        }
                    })
            }).catch(() => {
                console.log("Auto sign in was not possible")
            })
    };
}


/**
 * Log out user and remove user information from local storage and Redux
 */
export const logout = () => {
    //https://auth.dataporten.no/logout
    return dispatch => {
        console.log('REMOVE EVERYTHING FROM STORAGE');
        dispatch(clearStorage());
        dispatch(removeToken());
        dispatch(removeAffiliation());
        dispatch(removeLecturer())
        dispatch(removeStudent())
        dispatch(removeCourses())
        // Redirect user to auth-screen
        Navigation.startSingleScreenApp({
            screen: {
                screen: "regapp.AuthScreen"
            },
            passProps: {
                hasLoggedOut: true
            },
            appStyle: {
                navBarHidden: true
            },
        });

    }
}

/**
 * Remove user affiliation from Redux
 */
export const removeAffiliation = () => {
    return {
        type: AUTH_REMOVE_AFFILIATION
    }
}

/**
 * This function will store the affiliation of the logged in user in Redux 
 * @param {The affiliation of the logged in user} affiliation 
 */
export const setAffiliation = affiliation => {
    return {
        type: AUTH_SET_AFFILIATION,
        affiliation: affiliation
    };
};


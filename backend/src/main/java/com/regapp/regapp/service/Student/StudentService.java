package com.regapp.regapp.service.Student;

import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;

import java.util.List;

public interface StudentService {

    List<Student> getStudents();

    Student getStudentById(Long studentID);

    Student getStudentByUserid(String userid);

    List<Requirement> getAllRequirementsForStudent(Long studentID);

    Boolean isRequirementSignedForStudent(Long studentID, Long requirementID);

    Student updateStudent(Long studentID, Student studentDetails);
    Boolean  checkIfUserIsStudent(String email);

    void deleteStudent(Long studentId);

    Student setFeideIdToStudent(String feideId);

}

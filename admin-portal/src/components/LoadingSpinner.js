import React from 'react'
import { Container } from 'reactstrap';
import Spinner from 'react-spinkit';

const LoadingSpinner = () => (
    <Container className="App">
        <div className="Spinner">
            <Spinner name="folding-cube" color="dark" fadeIn="none" />
        </div>
    </Container>


)
export default LoadingSpinner; 
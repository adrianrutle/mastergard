import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoadingSpinner from '../components/LoadingSpinner';
import { getCourseLecturers, selectLecturer, removeLecturerFromCourse } from '../store/actions'
import { Button, Container, Table, Input, ButtonGroup } from 'reactstrap';
import { browserHistory } from '../App'
import RemoveLecturerModal from '../components/CustomModal';



export class Lecturers extends Component {
  state = {
    lecturerSearch: '',
    availableLecturers: [],
    filteredAvailableLecturers: [],
    courseLecturers: [],
    lecturerToRemove: ''
  }

  componentDidMount() {
    this.props.getCourseLecturers(this.props.selectedCourse.id)
    /* this.props.getAvailableLecturers() */
  }
  componentWillReceiveProps() {
    if (this.props.lecturers.courseLecturers.length > 0) {
      this.setState({
        courseLecturers: this.props.lecturers.courseLecturers
      })
    }
    if (this.props.lecturers.availableLecturers.length > 0) {
      this.setState({
        availableLecturers: this.props.lecturers.availableLecturers,
        filteredAvailableLecturers: this.props.lecturers.availableLecturers
      })
    }
  }


  openRemoveLecturerModal(lecturerId) {
    this.setState({
      lecturerToRemove: lecturerId
    })
    this.modal.toggleModal()
  }

  addLecturerToCourse(lecturer) {

    this.props.selectLecturer(lecturer)
    browserHistory.push('/lecturers/select-requirements')
  }

  populateLecturersTable(lecturerData, courseLecturers) {
    if (lecturerData.length > 0) {
      return lecturerData.map(lecturer => {
        return <tr key={lecturer.id}>
          <td style={{ whiteSpace: 'nowrap' }}>{lecturer.name}</td>
          <td style={{ whiteSpace: 'nowrap' }}>{lecturer.email}</td>
          <td>
            {courseLecturers ?

              <ButtonGroup>
                <Button
                  color="danger"
                  onClick={() => this.openRemoveLecturerModal(lecturer.id)}>
                  Fjern fra emne
              </Button>
                <Button
                  onClick={() => console.log("skjer")}>
                  Aktiviteter
              </Button>
              </ButtonGroup>
              :
              <Button
                color="info"
                onClick={() => this.addLecturerToCourse(lecturer)}>
                Legg til
              </Button>
            }
          </td>
        </tr>

      });
    }
  }

  filterTable(value) {
    let filteredAvailableLecturers = this.state.availableLecturers
    filteredAvailableLecturers = filteredAvailableLecturers.filter((lecturer) => {
      let lecturerName = lecturer.name.toLowerCase()

      return lecturerName.indexOf(
        value.toLowerCase()) !== -1
    })
    this.setState({
      filteredAvailableLecturers: filteredAvailableLecturers
    })
  }

  courseLecturers() {
    if (this.props.lecturers.courseLecturers.length !== 0) {
      return (
        <div>
          <h1 className={"header"}>Undervisere tilkyttet {this.props.selectedCourse.title}</h1>

          <RemoveLecturerModal
            ref={instance => { this.modal = instance; }}
            title={"Fjern underviser fra " + this.props.selectedCourse.title}
            body={"Hvis en underviser blir fjernet fra " + this.props.selectedCourse.title +
              ", vil underviseren miste all tilknyttelse til dette faget, og kan dermed ikke signere aktiviteter for dette faget."}
            posButton={"Fjern underviser"}
            posButtonFunc={() => this.props.removeLecturerFromCourse(this.state.lecturerToRemove)}
            negButton={"Avbryt"}
            negButtonFunc={() => this.modal.toggleModal()}
          />

          <Table className="mt-4">
            <thead>
              <tr>
                <th width="33%">Navn</th>
                <th width="33%">Epost</th>
                <th width="33%">Egenskaper</th>
              </tr>
            </thead>
            <tbody>
              {this.populateLecturersTable(this.state.courseLecturers, true)}
            </tbody>
          </Table>

        </div>
      )

    }
  }

  addLecturer() {
    if (this.props.lecturers.availableLecturers.length !== 0) {
      return (
        <div>
          <h2 className={"header"}>Legg til en underviser</h2>
          <Input type="email"
            name="email"
            id="exampleEmail"
            onChange={(e) => this.filterTable(e.target.value)}
            placeholder="Søk etter en underviser" />

          <Table className="mt-4">
            <thead>
              <tr>
                <th width="33%">Navn</th>
                <th width="33%">Epost</th>
                <th width="33%">Egenskap</th>
              </tr>
            </thead>
            <tbody>
              {this.populateLecturersTable(this.state.filteredAvailableLecturers, false)}
            </tbody>
          </Table>
        </div>
      )
    }
  }



  render() {
    if (this.props.isLoading) {
      return (<LoadingSpinner />)
    }
    return (
      <div>
        <Container className={"App"}>
          {this.courseLecturers()}
          {this.addLecturer()}
        </Container>
      </div>
    );
  }
}



const mapStateToProps = (state) => {
  return {
    selectedCourse: state.courses.selectedCourse,
    isLoading: state.ui.isLoading,
    lecturers: state.lecturer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCourseLecturers: (courseId) => dispatch(getCourseLecturers(courseId)),
    selectLecturer: (lecturer) => dispatch(selectLecturer(lecturer)),
    removeLecturerFromCourse: (lecturerId) => dispatch(removeLecturerFromCourse(lecturerId))


  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Lecturers)


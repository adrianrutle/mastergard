package com.regapp.regapp.service.Requirement;

import com.regapp.regapp.exception.ResourceNotFoundException;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.model.jointable.StudReq;
import com.regapp.regapp.repository.RequirementRepository;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("RequirementService")
public class RequirementServiceImpl implements RequirementService {
    @Autowired
    RequirementRepository requirementRepository;

    @Override
    public List<Requirement> getAllRequirements() {
        return requirementRepository.findAll();
    }

    @Override
    public List<Student> getAllRelatedStudents(Long requirementId) {
        Requirement requirement = requirementRepository.findById(requirementId)
                .orElseThrow(() -> new ResourceNotFoundException("Requirement", "requirementID", requirementId));
        return requirement.getAllStudents();
    }

    @Override
    public String getRequirementData(Long requirementId) {
        Requirement requirement = requirementRepository.findById(requirementId)
                .orElseThrow(() -> new ResourceNotFoundException("Requirement", "requirementID", requirementId));
        JSONArray jsonArray = new JSONArray();
            for (StudReq studReq : requirement.getStudReqs()) {

                JSONObject jsonObject = new JSONObject();
                    String studentName = studReq.getStudent().getName();
                    String studentEmail = studReq.getStudent().getEmail();
                    String studentGroup = studReq.getStudentGroup();
                    Boolean isRequirementSigned = studReq.isRequirementSigned();
                jsonObject.put("studentName",studentName);
                jsonObject.put("studentEmail",studentEmail);
                jsonObject.put("studentGroup",studentGroup);
                jsonObject.put("isRequirementSigned",isRequirementSigned);
                jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        }
}

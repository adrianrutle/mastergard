import { getToken } from './token'
import { appConfig } from '../util'
import { showAlertWithFunc } from './ui'
import { logout } from './auth';
import { Alert } from "react-native";
import { SET_COURSES, REMOVE_COURSES } from "./actionTypes";

export const getAllCourses = () => {
    return dispatch => {

        dispatch(getToken())
            .then(token => {
                fetch(appConfig.backendUrl + 'course', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": "Bearer " + token
                    }
                }).catch(err => {
                    console.log(err);
                })
                    .then(res => res.json())
                    .then(res => {
                        //FIX TODO
                        if (res.error === 'invalid_token') {
                            dispatch(showAlertWithFunc(
                                "Advarsel",
                                "Det skjedde noe galt, vennligst logg inn på nytt",
                                "LOGOUT"
                            ))
                            return null;

                        }
                        const courses = [];
                        for (let key in res) {
                            courses.push({
                                ...res[key]
                            })
                        }

                        dispatch(setCourses(courses));
                    })
            })
    };
}

export const setCourses = courses => {
    return {
        type: SET_COURSES,
        courses: courses
    }
};

export const removeCourses = () => {
    return {
        type: REMOVE_COURSES
    }
}
import {
    SET_REQUIREMENTS,
    REMOVE_REQUIREMENTS,
    SELECTED_REQUIREMENT,
    SELECTED_REQUIREMENT_DATA
} from "../actions/actionTypes";
const initialState = {
    requirements: [],
    selectedRequirement: {}
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_REQUIREMENTS:

            return {
                ...state,
                requirements: action.requirements
            };
        case SELECTED_REQUIREMENT:
            return {
                ...state,
                selectedRequirement: action.requirement
            };

        case SELECTED_REQUIREMENT_DATA:
            let selectedRequirement = state.selectedRequirement
            selectedRequirement['requirementData'] = [action.requirementData][0];
            return {
                ...state,
                selectedRequirement: selectedRequirement
            };
        case REMOVE_REQUIREMENTS:
            return {
                ...state,
                requirements: [],
                selectedRequirement: {}
            };
        default:
            return state;
    }
};

export default reducer;

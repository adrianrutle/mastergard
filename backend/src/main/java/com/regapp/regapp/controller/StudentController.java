package com.regapp.regapp.controller;

import com.regapp.regapp.model.Student;
import com.regapp.regapp.service.DataService;
import com.regapp.regapp.service.Student.StudentService;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
@RestController
public class StudentController {
    @Autowired
    StudentService studentService;

    @Autowired
    DataService dataService;

    /**
     * Used
     */
    @GetMapping("/student/result")
    public void getCoursesData(HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", "attachment; filename=RegApp.zip");
        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());
        for (File file : dataService.getStudentResultsFiles()) {
            zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
            FileInputStream fileInputStream = new FileInputStream(file);
            IOUtils.copy(fileInputStream, zipOutputStream);
            fileInputStream.close();
            zipOutputStream.closeEntry();
        }
        zipOutputStream.close();
    }

    @PostMapping("/student/feide/{feideId}")
    public Student setFeideIdToStudent(@PathVariable(value = "feideId") String feideId) {
        return studentService.setFeideIdToStudent(feideId);
    }
}

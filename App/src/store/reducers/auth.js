import {
    AUTH_SET_TOKEN,
    AUTH_REMOVE_TOKEN,
    AUTH_SET_AFFILIATION,
    AUTH_REMOVE_AFFILIATION
} from "../actions/actionTypes";

const initialState = {
    token: null,
    affiliation: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_SET_TOKEN:
            return {
                ...state,
                token: action.token
            };
        case AUTH_REMOVE_TOKEN:
            return {
                ...state,
                token: null
            };
        case AUTH_SET_AFFILIATION:
            return {
                ...state,
                affiliation: action.affiliation
            };
        case AUTH_REMOVE_AFFILIATION:
            return {
                ...state,
                affiliation: null
            };
        default:
            return state;
    }
};

export default reducer;

import React, { Component } from "react";
import {
  View, StyleSheet, Text, Image, ActivityIndicator, Dimensions, WebView
} from 'react-native';

import { connect } from "react-redux";
import { autoSignIn, doAuth } from "../../store/actions";
import ButtonWithBackground from "../../components/UI/Button/ButtonWithBackground";
import AlertHandler from "../../components/AlertHandler/AlertHandler";

class Auth extends Component {

  componentWillMount() {
    if (this.props.hasLoggedOut === undefined) {
      this.props.autoSignIn()
    }
  }
  componentDidMount() {
    //This will run when Feide autorization was completed.
    if (this.props.accessToken !== undefined) {
      this.props.doAuth(this.props.accessToken);
    }
  }

  authWithFeide = () => {
    this.props.navigator.push({
      screen: "regapp.FeideScreen",
      title: "Autentisering med Feide",
    });
  }
  renderButton() {
    if (this.props.isLoading) {
      return (
        <ActivityIndicator size="large" color="#4bacb8" />
      )
    } else {
      return (
        <ButtonWithBackground
          style={styles.button}
          onPress={this.authWithFeide}
        >
          <Text>Logg inn med Feide {this.props.isLoading}</Text>
        </ButtonWithBackground>
      )
    }
  }
  handelLogout = () => {
    if (this.props.hasLoggedOut === true) {
      return (
        <View style={{ height: 0 }}>
          <WebView
            source={{ uri: 'https://auth.dataporten.no/logout' }}
          />
        </View>
      )
    }
  }
  render() {
    return (
      <View style={styles.container}>
        {this.handelLogout()}
        <AlertHandler />
        <View style={styles.logoContainer}>
          <Image
            style={{ width: Dimensions.get('window').width, height: 250 }}
            source={require('../../assets/app-promo-auth.png')
            }
          />

        </View>
        <View style={styles.buttonContainer}>
          {this.renderButton()}
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#b4ffff'
  },
  logoContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#b4ffff'
  },
  buttonContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#b4ffff'
  },
  button: {
    width: 200,
    height: 80,
    backgroundColor: '#4bacb8',
  }
});

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    alert: state.ui.alert
  }
};

const mapDispatchToProps = dispatch => {
  return {
    autoSignIn: () => dispatch(autoSignIn()),
    doAuth: (accessToken) => dispatch(doAuth(accessToken)),

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);

import { showAlert, uiStopLoading } from './ui'
import { appConfig, storeArrayInStorage, clearStorage, getItemFromStorage } from '../util'
import { SET_STUDENT, REMOVE_STUDENT } from "./actionTypes";
import startMainTabs from '../../screens/MainTabs/startMainTabs'
export const fetchStudent = accessToken => {
    return (dispatch, getState) => {
        fetch(appConfig.userInfoUrl, {
            method: 'GET',
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + accessToken
            }
        }).catch(err => {
            console.log(err);
        })
            .then(res => res.json())
            .then(student => {
                dispatch(setFeideIdToStudent(student.user.userid, accessToken))
            });
    }
}


export const setFeideIdToStudent = (feideId, accessToken) => {
    return (dispatch) => {
        fetch(appConfig.backendUrl + 'student/feide/' + feideId, {
            method: 'POST',
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + accessToken
            }
        }).then(res => {
            res.json().then(studentToStore => {
                dispatch(uiStopLoading())
                dispatch(storeStudent(studentToStore));
                startMainTabs("ROLE_STUDENT")
            }
            )
        })
    }
}

export const getStudent = () => {
    return (dispatch, getState) => {
        const promise = new Promise((resolve, reject) => {
            const student = getState().user.student;
            if (!student) {
                getItemFromStorage('student:user')
                    .catch(err => reject())
                    .then(jsonStudent => {
                        var student = JSON.parse(jsonStudent)
                        if (!student) {
                            reject();
                            return;
                        }
                        dispatch(setStudent(student));
                        resolve(student);
                    })
            } else {
                resolve(student);
            }
        });
        promise.catch(err => {
            dispatch(clearStorage())
        });
        return promise;
    };
}

export const storeStudent = user => {
    var studentInfo = {
        name: user.name,
        email: user.email,
        userid: user.userid,
        id: user.id
    }
    return dispatch => {
        dispatch(setStudent(studentInfo));
        storeArrayInStorage('student:user', studentInfo)
    }
}
export const setStudent = student => {
    return {
        type: SET_STUDENT,
        student: student
    }
};

export const removeStudent = () => {
    return {
        type: REMOVE_STUDENT
    }
}


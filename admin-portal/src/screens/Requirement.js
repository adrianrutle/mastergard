import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Table, Input, Row, Col } from 'reactstrap';
import LoadingSpinner from '../components/LoadingSpinner';

import { getRequirementData } from '../store/actions'
import LineChart from '../components/Charts/LineChart';
import PieChart from '../components/Charts/PieChart';
export class Requirement extends Component {
    state = {
        requirementSearch: '',
        requirementsData: [],
        filteredRequirementsData: []
    }
    componentDidMount() {
        this.props.getRequirementData()
        if (this.props.selectedRequirementData !== undefined) {
            this.setState({
                requirementsData: this.props.selectedRequirementData,
                filteredRequirementsData: this.props.selectedRequirementData,
            })
        }

    }
    componentWillReceiveProps() {
        if (this.props.selectedRequirementData !== undefined) {
            this.setState({
                requirementsData: this.props.selectedRequirementData,
                filteredRequirementsData: this.props.selectedRequirementData,
            })
        }
    }
    populateRequirementTable(requirementData) {
        if (requirementData.length > 0) {
            return requirementData.map((data, i) => {
                return <tr key={i}>
                    <td style={{ whiteSpace: 'nowrap' }}>{data.studentName}</td>
                    <td style={{ whiteSpace: 'nowrap' }}>{data.studentEmail}</td>
                    <td style={{ whiteSpace: 'nowrap' }}>{data.studentGroup}</td>
                    <td style={{ whiteSpace: 'nowrap' }}>{data.isRequirementSigned ? "Signert" : "Ikke signert"}</td>
                </tr>
            });
        }
    }
    filterTable(value) {
        let filteredRequirementsData = this.state.requirementsData
        filteredRequirementsData = filteredRequirementsData.filter((data) => {
            let requirementTitle = data.studentName.toLowerCase()
            return requirementTitle.indexOf(value.toLowerCase()) !== -1
        })
        this.setState({
            filteredRequirementsData: filteredRequirementsData
        })
    }

    requirementDataTable() {
        if (this.props.selectedRequirementData !== undefined) {
            return (
                <div>
                    <Input type="email"
                        name="email"
                        id="exampleEmail"
                        onChange={(e) => this.filterTable(e.target.value)}
                        placeholder="Søk etter en student" />

                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th width="25%">Studentnavn</th>
                                <th width="25%">E-post</th>
                                <th width="25%">Gruppe</th>
                                <th width="25%">Er aktivitet signert</th>

                            </tr>
                        </thead>
                        <tbody>
                            {this.populateRequirementTable(this.state.filteredRequirementsData)}
                        </tbody>
                    </Table>
                </div>
            )
        } else {
            return (
                <div>
                    <h2>Ingen aktiviteter tilgjengelig</h2>
                </div>
            )
        }
    }


    render() {
        if (this.props.selectedRequirement === undefined ||
            this.props.selectedRequirementData === undefined ||
            this.props.isLoading === true) {
            return (<LoadingSpinner />)
        }
        return (
            <Container className={"App"}>
                <h2 className={"header"}>{this.props.selectedRequirement.title}</h2>
                <Row className="row">
                    <Col>
                        <LineChart data={"skjer"}></LineChart>
                    </Col>
                    <Col>
                        <PieChart data={this.props.selectedRequirementData}></PieChart>
                    </Col>
                </Row>
                <div style={{ marginTop: 20 }}>
                    {this.requirementDataTable()}
                </div>


            </Container>
        )
    }
}

const mapStateToProps = (state) => ({
    selectedRequirement: state.requirements.selectedRequirement,
    selectedRequirementData: state.requirements.selectedRequirement.requirementData,
    //  requirements: state.requirements.requirements,
    isLoading: state.ui.isLoading
})

const mapDispatchToProps = (dispatch) => {
    return {
        getRequirementData: () => dispatch(getRequirementData()),
        // getRequirementForCourse: () => dispatch(getRequirementForCourse())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Requirement)

package com.regapp.regapp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.regapp.regapp.model.jointable.StudReq;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "STUDENT")
public class Student {
    @Id
    @GeneratedValue
    @Column(name = "STUDENT_ID")
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String email;

    private String userid;

    @OneToMany(
            mappedBy = "primaryKey.student",
            cascade = CascadeType.ALL,
            orphanRemoval = true)

    @JsonBackReference
    private List<StudReq> studReqs = new ArrayList<>();

    public Student() {
    }

    public Student(@NotBlank String name, @NotBlank String email, List<StudReq> studReqs) {
        this.name = name;
        this.email = email;
        this.studReqs = studReqs;
    }

    public Student(@NotBlank String name, @NotBlank String email, @NotBlank String userid) {
        this.name = name;
        this.email = email;
        this.userid = userid;
    }

    public Student(@NotBlank String name, @NotBlank String email) {
        this.name = name;
        this.email = email;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public List<StudReq> getStudReqs() {
        return studReqs;
    }

    public void addStudRequirement(StudReq studReq) {
        this.studReqs.add(studReq);
    }

    @JsonIgnore
    public List<Requirement> getAllRequirements() {
        List<Requirement> requirements = new ArrayList<>();
        for (StudReq sr : getStudReqs()) {
            requirements.add(sr.getRequirement());
        }
        return requirements;
    }
}

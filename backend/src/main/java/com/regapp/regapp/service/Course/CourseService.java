package com.regapp.regapp.service.Course;

import com.regapp.regapp.model.Course;
import com.regapp.regapp.model.Lecturer;
import com.regapp.regapp.model.Requirement;
import com.regapp.regapp.model.Student;
import com.regapp.regapp.model.jointable.StudReq;
import org.json.JSONArray;

import java.util.List;

public interface CourseService {
    List<Course> getCourses();

    Course addCourse(String courseTitle,List<Requirement> requirements, List<Lecturer> lecturers, List<StudReq> studReqs);

    Course getCourseById(Long courseId);

    List<Requirement> getCourseRequirements(Long courseId);

    List<Student> getCourseStudents(Long courseId);

    List<Lecturer> getCourseLecturers(Long courseId);

    void addRequirementToCourse(Long courseId, Requirement requirement);

    void deleteRequirementFromCourse(Long courseId, Long requirementId);

    void deleteCourse(Long courseId);

    Course updateCourse(Long courseId, Course course);

    List<StudReq> getCourseRequirementsForStudent(Long courseId, Long studentId);

    List<Requirement> getCourseRequirementsForLecturer(Long courseId, Long lecturerId);

    Lecturer removeLecturerFromCourse(Long courseId, Long lecturerId);

}
